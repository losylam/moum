from flask import Flask, render_template
from flask_socketio import SocketIO, emit

app = Flask(__name__)
app.config['SECRET_KEY'] = "c'est un secret!"
socketio = SocketIO(app)


@app.route('/')
def index():
    return render_template('index.html')


@socketio.on('message')
def handle_message(message):
    print('received message: ',)
    print(message)
    emit('message', 'hello from flask')


if __name__ == '__main__':
    socketio.run(app, debug=True)
