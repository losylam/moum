const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');


module.exports = mode => {
    let base = {
        entry: path.resolve(__dirname, './src/index.js'),
        mode: 'production',
        devServer: {
            contentBase: './dist',
        },
        output: {
            filename: 'bundle.[contenthash].js',
            path: path.resolve(__dirname, 'dist'),
        },
        plugins:
        [
            new HtmlWebpackPlugin({
                template: path.resolve(__dirname, './src/index.html'),
                minify: true
            }),
        ],
        module: {
            rules: [
                {
                    // HTML
                    test: /\.(html)$/,
                    use: ['html-loader']
                },
                {
                    // JS
                    test: /\.js$/,
                    include: path.resolve(__dirname, 'src'),
                    exclude: /node_modules/,
                    use:
                    [
                        'babel-loader'
                    ]
                },
                {
                    // CSS
                    test: /\.css$/,
                    use: [
                        'style-loader',
                        'css-loader',
                    ],
                },
                {
                    // Images
                    test: /\.(png|svg|jpg|gif|otf|exr)$/,
                    loader: 'file-loader',
                    options: {
                        name: '[path][name].[ext]',
                        outputPath: (file) => {
                            let path = file.split("src/")[1];
                            return path;
                        }
                    }
                },
            ],
        },
    };

    if (mode == 'development'){
        base.mode = 'development';
        base.devtool = 'inline-source-map';
        base.cache = true;
    }

    return base;
};

