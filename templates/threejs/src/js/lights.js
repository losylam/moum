import { AmbientLight, DirectionalLight } from 'three';

export default class Light {
    constructor(scene){
        this.scene = scene;

        this.init();
    }

    init(){
        this.ambientLight = new AmbientLight(0xffffff);
        // Directional light
        this.directionalLight = new DirectionalLight(0xee0000, 0.5);
        this.directionalLight.position.set(1, 1, 1);

        this.scene.add(this.ambientLight);
        this.scene.add(this.directionalLight);
    }

}
