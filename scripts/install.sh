#!/bin/sh

DIRECTORY=`dirname $0`
ABSPATH=`realpath $DIRECTORY`
cd $DIRECTORY


rm -f moum
touch moum

echo "#!/bin/sh" >> moum
echo "sh $ABSPATH/run.sh -p \$PWD \$@" >> moum

chmod +x moum

mv moum ~/.local/bin

