#!/bin/sh

DIRECTORY=`dirname $0`
cd $DIRECTORY/..

. ./.venv/bin/activate

if [ $# -eq 2 ]
then
    python -m IPython -i scripts/start.py
else
    python scripts/start.py $@
fi

