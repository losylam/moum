import os
from pprint import pprint

from moum import Moum, PROJECT_ROOT
from moum.dirs import read_dir_csv
from moum.project import ProjectData
from moum.utils import create_hash

DIRS_CSV = '/home/laurent/Projets/Moum/data/dirs.csv'

entries = read_dir_csv(DIRS_CSV)

moum = Moum()

for entrie in entries:
    # pprint(entrie)
    uid = create_hash()
    path = os.path.join(PROJECT_ROOT, entrie['name'])

    parent = entrie['project']
    if parent == '':
        parent = None

    project_tags = [entrie['type']] + entrie['category']

    if os.path.isdir(path):
        print('project init', entrie['name'])
        p = ProjectData(uid=uid,
                    name=entrie['name'],
                    path=path,
                    status=entrie['status'],
                    parent_project = parent,
                    tags=entrie['tags'],
                    project_tags = project_tags,
                    year=entrie['year'])
        moum.add_project_data(p)
        # pprint(p)
    else:
        print('!!!!!!!!!! project not found', entrie['name'])
# moum = Moum(JSON_PROJECT)



