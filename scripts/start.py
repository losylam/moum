import sys
import os

from moum import Moum
from moum.site import reload_static

from livereload import Server
from IPython import embed

SITE_EXPORT = '/home/laurent/Projets/Site/moum-export'
SITE_EXPORT_DEV = './test/output/moum-export'

moum = Moum()


if 'build' in sys.argv[1:]:
    moum.build()

elif 'refresh' in sys.argv[1:]:
    moum.refresh()

elif 'publish' in sys.argv[1:]:
    moum.publish()

elif 'live' in sys.argv[1:]:
    server = Server()
    server.watch('./moum/static', moum.refresh)
    server.watch('./moum/templates', moum.build)
    server.serve(root=SITE_EXPORT)

elif 'live-local' in sys.argv[1:]:
    print(sys.argv[2])
    server = Server()
    server.watch(sys.argv[2], moum.refresh)
    server.serve(root=SITE_EXPORT)

elif 'activate' in sys.argv[1:]:
    # print(sys.argv[2])
    project = moum.activate(sys.argv[2])
    if project and len(project._posts) == 1:
        post = project._posts[0]
    os.chdir(sys.argv[2])
    embed(colors="neutral")

elif 'select' in sys.argv[1:]:
    selection = moum.select_post()
    if isinstance(selection, list) and len(selection) > 0:
        selection = selection[0]

    if selection:
        os.chdir(selection.path)
        if selection.category == 'post':
            post = selection
        elif selection.category == 'project':
            project = selection
        embed(colors="neutral")

elif 'dev' in sys.argv[1:]:
    moum = Moum(root='./test/input/test_moum/')
    moum.build()
    server = Server()
    server.watch('./moum/static', moum.refresh)
    server.watch('./test/input', moum.build)
    server.watch('./moum/templates', moum.refresh)
    server.serve(root=SITE_EXPORT_DEV)

elif 'run' in sys.argv[1:]:
    moum.run()
