import os
import shutil
import subprocess
import tempfile
import json
from pprint import pprint

import xml.etree.ElementTree as ET

from moum.utils import time_to_json, create_hash

SITE_XML = '/home/laurent/Projets/Site/laurentmalys.WordPress.2023-09-12.xml'
OUTPUT = '/home/laurent/Projets/Site/moum-import'

PREFIX = "{http://wordpress.org/export/1.2/}"
tag_content = "{http://purl.org/rss/1.0/modules/content/}encoded"
tag_postmeta = PREFIX + 'postmeta'

def prefix(tag):
    return PREFIX + tag

def get_type(item):
    post_type = item.find(prefix('post_type'))
    return post_type.text

def import_from_wordpress_xml(filename_xml):
    tree = ET.parse(SITE_XML)
    root = tree.getroot()[0]
    items = root.findall('item')
    return items

def get_pubs(items):
    pubs = []
    for item in items:
        post_type = get_type(item)
        post_name = item.find(prefix('post_name')).text
        if post_type in ['page', 'post'] and post_name:
            pubs.append(item)
    return pubs

def get_medias(items):
    pubs = []
    for item in items:
        post_type = get_type(item)
        if post_type == 'attachment':
            pubs.append(import_media(item))
    return pubs

def get_medias_by_id(items):
    by_id = {}
    for item in items:
        by_id[item['post_id']] = item
    return by_id

def import_media(item):
    d = {}
    d['post_id'] = item.find(prefix('post_id')).text
    d['url'] = item.find(prefix('attachment_url')).text
    d['postmeta'] = get_postmeta(item)

    return d

def download_medias(items):
    media_folder = os.path.join(OUTPUT, 'medias')
    if os.path.exists(media_folder):
        shutil.rmtree(media_folder)
    os.makedirs(media_folder)
    for item in items:
        item_folder = os.path.join(media_folder, item['post_id'])
        os.makedirs(item_folder)
        subprocess.call([
            'wget',
            item['url'],
            '-P',
            item_folder
        ])

def import_pub(item):
    post_name = item.find(prefix('post_name')).text
    post_title = item.find('title').text
    print('import %s'%post_title)
    d = {}
    for key in ['title', 'link', 'pubDate']:
        d[key] = item.find(key).text
    d['content'] = item.find(tag_content).text
    d['post_date_gmt'] = time_to_json(item.find(prefix('post_date_gmt')).text)
    d['postmeta'] = get_postmeta(item)
    d['post_name'] = post_name
    d['status'] = item.find(prefix('status')).text
    return d

def parse_items(items):
    return [import_pub(item) for item in items]

def get_postmeta(item):
    fields = item.findall(tag_postmeta)
    meta = {}
    for field in fields:
        key =  field.find(prefix('meta_key')).text
        value = field.find(prefix('meta_value')).text
        meta[key] = value
    return meta

def export(pubs, folder):
    if not os.path.exists(folder) and os.path.isdir(folder):
        os.makedirs(folder)

    posts = os.path.join(folder, 'posts')
    if os.path.exists(posts):
        ok = input('remove %s ? [o/n] '% posts)
        if ok in 'yYoO':
            shutil.rmtree(posts)
            os.makedirs(posts)
        else:
            return

    for pub in pubs:
        if pub['post_name'] is not None:
            export_pub(pub, posts)

def export_pub(pub, posts_folder):
    path = os.path.join(posts_folder, pub['post_name'])
    os.makedirs(path)
    output_html = os.path.join(path, 'index.html')
    output_md = os.path.join(path, 'post.md')
    output_json = os.path.join(path, 'post.json')

    content_html = get_html(pub)
    with open(output_html, 'w') as f:
        f.write(content_html)

    if 'postmeta' in pub and '_thumbnail_id' in pub['postmeta']:
        thumb_folder = os.path.join(path, 'thumbnail')
        os.makedirs(thumb_folder)
        thumb_orig_folder = os.path.join(OUTPUT, 'medias', pub['postmeta']['_thumbnail_id'])
        thumb = os.listdir(thumb_orig_folder)[0]
        shutil.copy(os.path.join(thumb_orig_folder, thumb), thumb_folder)

    json.dump(pub, open(output_json, 'w'), indent=4)
    html_to_md(output_html, output_md)

def get_html(pub):
    txt = '<html>'
    txt += '  <head>'
    txt += '    <title>%s</title>' % pub['title']

    txt += '  </head>'
    txt += '  <body>'

    txt += '    <h1>%s</h1>' % pub['title']

    if pub['content'] is not None:
        txt += pub['content']
    txt += '  </body>'
    txt += '</html>'
    return txt

def html_to_md(in_html, out_md):

    with tempfile.TemporaryDirectory() as tmpdir:
        tmpout = os.path.join(tmpdir, 'tmp.md')
        com = [
            'pandoc',
            in_html,
            '-r',
            'html+raw_html',
            '-w',
            'gfm',
            '--wrap=none',
            # '+raw_html',
            '-o',
            tmpout
        ]
        try:
            subprocess.call(com)
            with open(tmpout, 'r') as md:
                txt = md.read()
                # txt = txt.replace('\n\n', '%nn%')
                # txt = txt.replace('\n', ' ')
                # txt = txt.replace('%nn%', '\n\n')
                with open(out_md, 'w') as final_md:
                    final_md.write(txt)
        except:
            print('html_to_md failed', in_html)

def export_to_moum():
    pub_to_json = json.load(open(os.path.join(OUTPUT, 'pub_to_project.json')))
    pub_to = pub_to_json['pub_to_project']
    for pub in pub_to:
        pub_to_moum(pub)

def pub_to_moum(pub):

    # print(pub['pub'])
    dic = {}
    path_pub = os.path.join(OUTPUT, 'posts', pub['pub'])
    pub_data = json.load(open(os.path.join(path_pub, 'post.json')))
    path_project = os.path.join('/home/laurent/Projets', pub['project'])
    path_posts = os.path.join(path_project, 'moum', 'posts')
    path_post = os.path.join(path_posts, pub_data['post_name'])
    if os.path.exists(path_posts):
        shutil.rmtree(path_posts)
    os.makedirs(path_post)

    dic['uid'] = create_hash()
    dic['name'] = pub_data['post_name']
    dic['title'] = pub_data['title']
    dic['path'] = path_post
    dic['project'] = pub['project']
    dic['post_date'] = pub_data['post_date_gmt']
    dic['status'] = 'published'

    path_thumbnail = os.path.join(path_pub, 'thumbnail')
    if os.path.isdir(path_thumbnail):
        copy_thumbnail(path_thumbnail, path_post)
        name_thumbnail = os.listdir(path_thumbnail)[0]
        dic['thumbnail'] = './thumbnail/%s' % name_thumbnail

    shutil.copy(os.path.join(path_pub, 'post.md'), dic['path'])
    if 'nom_galerie' in pub_data['postmeta']:
        path_gallery = os.path.join(
            '/home/laurent/Projets/Site/www/my-content/gallery',
            pub_data['postmeta']['nom_galerie']
        )
        if os.path.isdir(path_gallery):
            path_medias = os.path.join(dic['path'], 'medias')
            out_gallery = os.path.join(path_medias, 'gallery')
            os.makedirs(path_medias)
            dic['gallery'] = './medias/gallery'
            pprint(dic)
            shutil.copytree(path_gallery, out_gallery)
    path_json = os.path.join(dic['path'], 'moum-post.json')
    json.dump(dic, open(path_json, 'w'), indent=4)



def copy_thumbnail(path_thumbnail, path_post):
    out = os.path.join(path_post, 'thumbnail')
    print('o===>', out)
    shutil.copytree(path_thumbnail, out)

if __name__=="__main__":
    # items = import_from_wordpress_xml(SITE_XML)
    # pub_items = get_pubs(items)
    # medias = get_medias(items)
    # medias_by_id = get_medias_by_id(medias)
    # ## download_medias(medias)
    # pubs = parse_items(pub_items)
    # export(pubs, OUTPUT)
    export_to_moum()

