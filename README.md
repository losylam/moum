# Moum

Moum is a python tool to manage developpement projects using gitlab and emacs org-mode

## Principle

- Create links between gitlab projects and org-mode entries
- Store data in a json database
- Read, create and update issues from the command line and synchronise with org-mode entries
