#!/usr/bin/python
# coding: utf-8

import orgparse
from moum.orgclock import get_clocked, get_project_clocks

def test_log():
    root = orgparse.load('test/input/test_clock.org')
    # for children in root.children:
    #     for node in children:
    #         print(get_node_path(node), node.clock)

    clocked = get_clocked(root)
    print(clocked)
    for i in clocked:
        print(i['path'], i['start'], i['end']-i['start'])


def test_clock_proj():
    n = get_project_clocks('/home/laurent/Org/projets.org', 'camionconversationnel')
    print(n)
