from pathlib import Path

from moum.site import gfm_to_html, apply_template, export_index
from moum.post import Post
from test_post import postdata
from test_moum import load_moum

md_content = """
# Title

Some Content

and [a link](https://www.laurent-malys.fr)

<div class="test_class"> Some html </div>
"""


def test_gfm_to_html():
    txt = gfm_to_html(md_content)
    print(txt)
    assert 'Some Content' in txt
    assert '</div>' in txt
    assert 'test_class' in txt


def test_content_template():
    txt = apply_template('content/content-test.html')
    assert 'Test content template' in txt


def test_content_template_list():
    data = [
        {"title": "Elem 1"},
        {"title": "Elem 2"},
    ]
    txt = apply_template('content/content-test-list.html', {"data": data})
    assert "Elem 1" in txt
    # print(txt)
    # assert 'Test content template' in txt


def test_content_template_moum():
    moum = load_moum()
    posts = moum.filter('post', 'post')
    data = [post.get_data() for post in posts]
    txt = apply_template('content/content-test-list.html', {"data": data})
    print(txt)


def test_export_post():
    p = Post(**postdata)

    moum = load_moum()
    p.set_moum(moum)
    p.export_html()


def test_export_post_from_folder():
    p = Post.load('test/input/test_post')

    moum = load_moum()
    p.set_moum(moum)
    p.export_html()


def test_create_index():
    p = Post(**postdata)
    out = Path('test/output')
    posts = [p]

    moum = load_moum()

    export_index(moum, posts, out)


def test_export_site_default():
    moum = load_moum()
    moum.build()


def test_export_site_publish():
    moum = load_moum()
    moum.publish()


def test_export_site_path():
    path = Path('test/output/moum-export-path')
    moum = load_moum()
    moum.tmp_export = path
    moum.build()
