from pathlib import Path

from moum.tag import Tags, Tag

from test_moum import load_moum


def test_tag_create():
    new_tag = Tag.create('tag')
    assert isinstance(new_tag, Tag)
    assert new_tag.label == 'tag'

    tag_dict = {
        "uid": 000,
        "name": "a-tag",
        "label": "a tag"
    }
    new_tag = Tag(**tag_dict)
    assert isinstance(new_tag, Tag)


def test_tag_serialize():
    new_tag = Tag.create('tag')
    assert new_tag.serialize()['name'] == 'tag'


def test_tags_create():
    tags = Tags()
    assert isinstance(tags, Tags)


def test_tags_add():
    tags = Tags()

    tags.append('test-tag')
    assert len(tags) == 1
    assert isinstance(tags['test-tag'], Tag)

    new_tag = Tag.create('another-tag')
    tags.append(new_tag)
    assert len(tags) == 2
    assert isinstance(tags['another-tag'], Tag)

    tags.append(new_tag)
    tags.append('test-tag')
    assert len(tags) == 2
    assert isinstance(tags['another-tag'], Tag)


def test_tags_save():
    path = Path('test/output/tags.json')
    tags = Tags()
    tags.append('test-tag')
    tags.save(path)


def test_tags_moum():
    moum = load_moum()
    tags = Tags(moum)

    assert len(tags) == 3
    assert 'a-tag' in tags
    assert len(tags['a-tag'].posts()) == 3
    assert len(tags['another-tag'].posts()) == 3
