#!/usr/bin/python
# coding: utf-8

# import pytest
import os
import filecmp

from moum.orgfile import OrgNode, OrgFile
from moum.orgfile import parse_propertie, parse_tags, parse_todo, parse_orgfile_heading


def test_parse_propertie():
    key, value = parse_propertie("  :KEY:  value\n")
    assert key == "KEY"
    assert value == "value"

    blank_key, blank_value = parse_propertie(" KEY: value\n")
    assert blank_key is None
    assert blank_value is None


def test_parse_tags():
    tag = parse_tags("*** heading   :tag:\n")
    assert tag == ["tag"]

    tags = parse_tags("*** heading   :tag1:tag2:\n")
    assert tags == ["tag1", "tag2"]

    blank_tag = parse_tags("*** heading :tag1:tag2\n")
    assert blank_tag is None


def test_parse_todo():
    assert parse_todo('** TODO a todo') == 'TODO'
    assert parse_todo('** not a todo') is None
    assert parse_todo('*** OPENED', ['TODO', 'OPENED']) == 'OPENED'
    assert parse_todo('*** OPENED', ['TODO']) is None


def test_orgnode_get_tags():
    o = OrgNode(0, heading="*** heading :tag: \n")
    o.get_tags()

    assert o.tags == ["tag"]


def test_orgnode_set_tags():
    o = OrgNode(0, heading="*** heading :tag: \n")
    o.get_tags()
    o.set_tag('tag2')

    o2 = OrgNode(0, heading="*** heading :tag: \n")
    o2.get_tags()
    o2.set_tag(['tag2', 'tag3'])

    assert 'tag2' in o.tags
    assert 'tag2' in o2.tags
    assert 'tag3' in o2.tags


def test_orgnode_get_title():
    o = OrgNode(0, heading="*** heading :tag: \n")
    o.get_meta()

    assert o.title == 'heading'


def test_orgnode_create_heading():
    o = OrgNode(0, heading="*** heading :tag: \n")
    o.get_meta()
    o.create_heading()

    assert len(o.heading) == 78


def test_orgnode_create_heading_tags():
    o = OrgNode(3, heading="*** heading :tag: \n")
    o.get_meta()
    o.set_tag('tag1')
    o.create_heading()

    assert o.heading == '*** heading                                                        :tag:tag1:\n'
    assert len(o.heading) == 78


def test_parse_orgfile_header():
    lines = ['#+ HEADING_TEST',
             '#+PROPERTIE: Value',
             '\n']

    properties = parse_orgfile_heading(lines)

    assert 'HEADING_TEST' in properties
    assert properties['PROPERTIE'] == 'Value'


def test_orgnode_get_properties():
    o = OrgNode(1, heading="empty")
    o.lines = [":PROPERTIES:\n",
               ":ID:   test\n",
               ":END:\n"]
    o.get_properties()
    assert o.properties["ID"] == "test"
    assert o.properties_pos == [0, 2]


def test_orgnode_set_property():
    o = OrgNode(1, heading="* empty\n")
    o.set_propertie('PROPERTIE', 'Value')

    assert o.properties_modified
    assert o.properties['PROPERTIE'] == 'Value'
    assert str(o) == '* empty\n  :PROPERTIES:\n  :PROPERTIE: Value\n  :END:\n'


def test_orgnode_change_properties():
    o = OrgNode(1, heading="empty")
    o.lines = [":PROPERTIES:\n",
               ":ID:   test\n",
               ":END:\n"]
    o.get_properties()
    o.set_propertie('PROPERTIE', 'Value')
    o.set_propertie('ID', 'changed')

    o2 = OrgNode(1, heading="empty")
    o2.lines = str(o).split('\n')[1:]
    o2.get_properties()

    assert 'PROPERTIE' in o2.properties
    assert o2.properties['PROPERTIE'] == 'Value'
    assert o2.properties['ID'] == 'changed'


def test_orgnode_str():
    o = OrgNode(0)
    assert str(o) == ""


def test_orgfile_byid():
    o = OrgFile("test/input/test_input.org")
    o.parse()
    assert "repo" in o.nodes_by_id


def test_orgfile_write():
    in_filename = "test/input/test_input.org"
    out_filename = 'test/input/test_output.org'
    o = OrgFile(in_filename)
    o.parse()
    o.write(out_filename)
    assert filecmp.cmp(in_filename, out_filename, shallow=False)


def test_orgfile_parse_heading():
    o = OrgFile("test/input/test_input.org")
    o.parse()
    assert 'HEADING_TEST' in o.heading
    assert o.heading['PROPERTIE'] == 'Value'


def test_orgfile_get_todos():
    o = OrgFile("test/input/test_input.org")
    o.parse()

    assert 'CANCELLED' in o.todos
    assert len(o.todos) == 5


def test_orgfile_get_todo():
    o = OrgFile("test/input/test_input.org")
    o.parse()

    assert o.root.children[0].todo == 'TODO'
    assert o.nodes_by_id['cancelled_heading'].todo == 'CANCELLED'
    assert o.nodes_by_id['todo_not_in_heading'].todo is None


def test_udpate_issues():
    o = OrgFile("test/input/test_input.org")
    o.parse()
    num_node = len(o.nodes)
    new_issues = [{"id": 1,
                  "title": "Issue Test",
                   "description": "This is just a test\n",
                   "state": "opened"}]
    o.update_issues(new_issues, "repo")
    o.write("test/input/test_update_issue.org")
    assert len(o.nodes) == num_node + 1


def test_magique():
    if os.path.exists("/home/laurent/Org/projets.org"):
        o = OrgFile("/home/laurent/Org/projets.org")
        o.parse()
        # o.write("test/input/test_magique.org")
        # assert filecmp.cmp("/home/laurent/Org/projets.org",
        #                    "test/input/test_magique.org",
        #                    shallow=False)
