#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Test Moum utils"""

# import os
# import tempfile
# import filecmp

from moum.utils import time_to_json


def test_time_to_json():
    t_string = '2023-09-14 13:55:50'

    t_json = time_to_json(t_string)

    assert t_json == '2023-09-14T13:55:50.000Z'
