#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Test Moum main class"""

import os
import shutil
import tempfile
import filecmp

from moum import Moum

def create_moum(tmpdir):
    moum_root = os.path.join(tmpdir)
    return Moum(root=moum_root)

def load_moum():
    return Moum(root='./test/input/test_moum')

def test_empty():
    moum = Moum(project_json='test/input/empty.json', config='test/input/config_empty')

    assert moum.data == {'projects': [], 'repos': []}

def test_create():
    with tempfile.TemporaryDirectory() as tmpdir:
        moum = create_moum(tmpdir)

def test_autosave():
    with tempfile.TemporaryDirectory() as tmpdir:
        moum = create_moum(tmpdir)

        moum.add_project('test/input/test_project')

        assert len(moum.projects_by_name) == 1
        assert len(moum.projects_list) == 1

        # print(open('test/input/projet_test.json', 'r').read())
        # print(open(moum.project_json, 'r').read())
        # shutil.copy(moum.project_json, 'test/input/projet_test.json')
        assert filecmp.cmp('test/input/projet_test.json',
                           moum.project_json)

def test_activate():
    with tempfile.TemporaryDirectory() as tmpdir:
        moum = create_moum(tmpdir)
        moum.add_project('test/input/test_project')
        moum.activate_project('test_project')

        assert moum.project['name'] == 'test_project'

def test_config():
    moum = load_moum()
    assert moum.config['site_export'] == "./test/output/moum-export"
