#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Test Moum project class"""

import os
import tempfile
import filecmp
import time

from pathlib import Path

from moum.utils import create_hash, time_to_json
from moum.project import Project

from test_moum import load_moum

projectdata = {
    "uid": create_hash(),
    "name": "TestProject",
    "label": "A Test Project",
    "path": "test/input/TestProject",
    "tags": ['tag1', 'tag2'],
    "year": 2042
}


def create_projectdata():
    return Project(**projectdata)


def test_create_projectdata():
    p = create_projectdata()
    assert p.name == 'TestProject'


def test_writeread_projectdata():
    p = create_projectdata()
    with tempfile.TemporaryDirectory() as tmpdir:
        path = os.path.join(tmpdir, 'test.json')

        p.to_json(path)

        p_read = Project.from_json(path)

        assert p.project_tags == []
        assert len(p.tags) == 2
        assert p.year == 2042
        assert p == p_read


def test_saveload_project():
    with tempfile.TemporaryDirectory() as tmpdir:
        projectdata["path"] = os.path.join(tmpdir, "TestProject")
        p = Project(**projectdata)
        p.save()

        p_load = Project.load(p.path)

        assert os.path.isfile(p._data_path)
        assert p_load == p


def test_repos():
    p = Project(uid=0, name="test", path="test/input/test_project")
    repos_paths = p.find_repos()
    repos = p.get_repos()

    assert len(repos_paths) == 1
    assert len(repos) == 1


def test_childproject():
    moum = load_moum()
    child = moum.projects_by_name['Child Project']
    parent = moum.projects_by_name['A Project']
    children_posts = parent.get_children_posts()
    # print('\nchildren', children_posts)
    # print('\nparent', parent._posts)
    assert child.parent_project == 'A Project'
    assert parent._child_projects[0].name == 'Child Project'
    assert len(children_posts) < len(parent._posts)
    # assert len(children_posts) > len(parent._posts)


def test_update():
    p = Project(uid=0, name="test", path="test/input/test_fullproject")
    p.update()

    change = p.check_change()
    p0 = p._st_mtime

    assert len(p.posts_paths) > 2
    assert not change

    # p.save()
    # with open(Path(p._data_dir) / 'tmp', 'w') as f:
    #     f.write(create_hash())

    # time.sleep(1)

    change_save = p.check_change()
    print(time_to_json(p._last_update), time_to_json(p._st_mtime))

    # WIP: cannot detech project has changed
    # assert change_save


# def test_check_change():
#     p = Project(uid=0, name="test")
