#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Test data"""

import os

from moum.data import JsonDict


def test_openempty():
    t = JsonDict('foo')

    assert t == {}


def test_read():
    t = JsonDict("test/input/input_json.json")

    assert t['test'] == 0


def test_write():
    filename = "test/output/output_json.json"
    if os.path.exists(filename):
        os.remove(filename)
    t = JsonDict(filename)
    t['test'] = 0

    children = {}
    children['0'] = 'first child'
    t['children'] = children
    t['children']['1'] = 'second child'
    # children = t['children']
    children['2'] = 'third child'

    t_out = JsonDict(filename)

    assert t_out['test'] == 0
    assert t_out['children']['0'] == 'first child'
    # assert t_out['children']['1'] == 'second child'
    # assert t_out['children']['2'] == 'third child'

    # os.remove(filename)
