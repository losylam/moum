#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Test Moum main class"""

import os
import tempfile
import filecmp

from moum.utils import create_hash
from moum.post import Post, find_posts

postdata = {
    "uid": create_hash(),
    "name": "name-of-the-post",
    "title": "A Title",
    "path": "test/input/test_post",
    "project": "AProject",
    "post_date": "2014-12-18T17:25:18.000Z",
    "status": "published",
    "thumbnail": "./thumbnail/thumbnail.jpg",
    "gallery": "./medias/gallery",
    "tags": ["tag", "test-tag"]
}


def test_create_post():
    # postdata = {
    #     "uid": create_hash(),
    #     "name": "test-post",
    #     "path": "input/test_post/"
    # }
    p = Post(**postdata)

    assert isinstance(p, Post)


def test_load():
    p = Post.load('test/input/test_post/')

    assert p.status == 'published'


def test_create_save():
    with tempfile.TemporaryDirectory() as tmpdir:
        postdata_ = {**postdata, 'path': tmpdir}
        p = Post(**postdata_)
        p.save()

        p_load = Post.load(tmpdir)

        assert p == p_load
        # p.save


def test_find():
    p = find_posts('test/input/test_fullproject')
    assert len(p) == 5

# def test_media():
#     p = Post.load('test/input/test_post/')

#     p.get_medias()
#     print(p._thumbnail)
#     assert p.status == 'published'


