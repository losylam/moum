#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Test Clips class"""

import os
import tempfile

from moum.clips import Clips


def test_empty():
    with tempfile.TemporaryDirectory() as tmpdir:
        clips_json = os.path.join(tmpdir, 'clips.json')
        clips = Clips(clips_json)
        clips.save_data()

        assert os.path.isfile(clips_json)


def test_addclip():
    with tempfile.TemporaryDirectory() as tmpdir:
        clips_json = os.path.join(tmpdir, 'clips.json')
        clips_json = os.path.join(tmpdir, 'clips.json')
        clips = Clips(clips_json)
        clips.add_entry('test/input/dumb.mp4', meta={
            'title': 'Dumb video'
        })

        assert len(clips.videos) == 1
        assert clips.videos[0]['title'] == 'Dumb video'
