#!/usr/bin/python
# coding: utf-8

"""Global config for Moum"""

import os

MOUM_ROOT = os.path.join(os.environ['HOME'], "Org/Moum")

MOUM_CACHE = os.path.join(MOUM_ROOT, "cache")
PROJECT_JSON = os.path.join(MOUM_ROOT, "projets.json")
CLIPS_JSON = os.path.join(MOUM_ROOT, "clips.json")
