#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Project generator"""

import os
import shutil
import subprocess
import json


PROJECT_TYPE = {
    'd3': {},
    'p5': {},
    'threejs': {},
    'flask': {},
}

WEBPACK_DEP = [
    "css-loader",
    "file-loader",
    "ignore-loader",
    "style-loader",
    "webpack",
    "webpack-cli",
    "webpack-dev-server",
    "html-webpack-plugin"
]

THREEWEBPACK_DEP = ["@babel/core", "babel-loader", "html-loader"]

WEBPACK_SCRIPTS = {
    "start": "webpack serve --open --env development",
    "build": "webpack --env production"
}

MOUM_DIR = os.path.dirname(os.path.abspath(__file__))
WEBPACK_CONFIG = os.path.join(MOUM_DIR, 'templates', 'webpack.config.js')


def init_git(directory):
    """init a new git repo"""
    subprocess.call([
        'git',
        'init',
        directory
    ])
    path = os.path.join(directory, '.gitignore')
    with open(path, 'w', encoding='utf-8') as f:
        f.write('node_modules\n')


def create_directory(directory):
    """create directory if not exists"""
    if os.path.exists(directory):
        print("Dir exists, aborting")
        return 0

    os.makedirs(directory)
    return 1


def create_sketch_d3(directory, webpack=True):
    """template for d3 sketch"""
    cur_dir = os.path.abspath('.')
    os.chdir(directory)

    os.mkdir('src')
    path = os.path.join('src', 'index.js')
    with open(path, 'w', encoding="utf-8") as f:
        f.write('import * as d3 from "d3";\n')

    subprocess.call(['npm', 'init', '--yes'])
    subprocess.call(['npm', 'install', 'd3'])
    if webpack:
        subprocess.call(['npm', 'install'] + WEBPACK_DEP)
        shutil.copy(WEBPACK_CONFIG, '.')

    pack_json = {}
    with open("package.json", 'r', encoding="utf-8") as f:
        pack_json = json.load(f)

    pack_json['author'] = "Laurent Malys"
    pack_json['version'] = '0.0.1'
    pack_json['main'] = 'src/index.js'

    if webpack:
        for k, it in WEBPACK_SCRIPTS.items():
            pack_json['scripts'][k] = it

    with open("package.json", 'w', encoding='utf-8') as f:
        json.dump(pack_json, f, indent=2)

    os.chdir(cur_dir)


def create_sketch_flask(directory):
    """template for flask"""
    cur_dir = os.path.abspath('.')

    template_dir = os.path.join(MOUM_DIR, "templates", "flask_socketio", ".")
    subprocess.call(["cp", "-a", template_dir, directory])

    os.chdir(directory)

    subprocess.call(["sh", "install.sh"])
    # subprocess.call(['python3', '-m', 'venv', 'venv'])
    # subprocess.call(['pip', 'install', '-r', 'requirements.txt'])

    os.chdir(cur_dir)


def create_sketch_threejs(directory):
    """template for threejs"""
    print("create threejs sketch")
    cur_dir = os.path.abspath('.')

    template_dir = os.path.join(MOUM_DIR, "templates", "threejs", ".")
    subprocess.call(["cp", "-a", template_dir, directory])

    os.chdir(directory)

    with open("package.json", 'r', encoding='utf-8') as f:
        pack_json = json.load(f)
    pack_json['author'] = "Laurent Malys"
    pack_json['version'] = '0.0.1'
    pack_json['main'] = 'src/index.js'
    pack_json['scripts'] = {}
    for k, it in WEBPACK_SCRIPTS.items():
        pack_json['scripts'][k] = it
        with open("package.json", "w", encoding='utf-8') as f:
            json.dump(pack_json, f, indent=2)

    subprocess.call(['npm', 'install', 'three', '--save'])
    lst_dep = WEBPACK_DEP + THREEWEBPACK_DEP
    subprocess.call(['npm', 'install', '--save-dev'] + lst_dep)
    # except:
    #     print("sketch creation failed")

    os.chdir(cur_dir)


def create_sketch_p5(directory):
    """template for p5"""
    cur_dir = os.path.abspath('.')

    template_dir = os.path.join(MOUM_DIR, "templates", "p5js", ".")
    subprocess.call(["cp", "-a", template_dir, directory])

    os.chdir(directory)

    os.chdir(cur_dir)


def create_sketch(directory, git=True, lib=None):
    """create sketch"""
    if os.path.exists(directory):
        print("Dir exists, aborting")
        return 0

    os.makedirs(directory)

    if git:
        init_git(directory)

    if lib in PROJECT_TYPE:
        if lib == 'd3':
            create_sketch_d3(directory)
        elif lib == 'p5':
            create_sketch_p5(directory)
        elif lib == 'threejs':
            create_sketch_threejs(directory)
        elif lib == 'flask':
            create_sketch_flask(directory)

    return 1
