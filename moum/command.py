"""Command utility for Moum"""

from pathlib import Path

import os
import subprocess


def im_resize(filein, fileout, width=None, height=None):
    """
    resize an image
    add filein suffix to fileout if not defined
    """
    path_out = Path(fileout)
    if path_out.suffix == '':
        path_out = Path(fileout + Path(filein).suffix)
    size = ''
    if width:
        size += str(width)
    size += 'x'
    if height:
        size += height
    cmd = [
        'convert',
        filein,
        '-resize',
        size,
        path_out
    ]

    return subprocess.call(cmd)


def edit(filename):
    """open in emacsclient"""
    com = [
        'emacsclient',
        '-r',
        str(filename)
    ]
    with subprocess.Popen(com, stdout=subprocess.PIPE) as p:
        p.wait()


def navigate(folder, app='emacs'):
    """navigate the folder"""
    # subprocess.Popen(['nautilus', str(folder)])
    if app == 'emacs':
        edit(str(folder))
    elif app in ['n', 'nautilus']:
        with subprocess.Popen(
                ['nautilus', str(folder)],
                stdout=subprocess.PIPE) as p:
            p.wait()
    elif app in ['r', 'ranger']:
        with subprocess.Popen(['ranger', str(folder)]) as p:
            p.wait()


def rsync(src, dst, verbose=True, delete_after=True):
    """sync src to dst using rsync"""
    cmd = [
        'rsync',
        '-a',
    ]
    if verbose:
        cmd.append('-v')
    if delete_after:
        cmd.append('--delete-after')
    cmd.append(src)
    cmd.append(dst)

    print('rsync command:', ' '.join(cmd))
    subprocess.call(cmd)

def run(base_dir, cmd):
    os.chdir(base_dir)
    subprocess.call(cmd.split())
