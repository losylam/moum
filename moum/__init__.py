#!/usr/bin/python
# coding: utf-8
"""
Moum main class
"""

import os
import json
from pathlib import Path

from .config import MOUM_ROOT
from .data import JsonDict
from .walk_git import get_repo_data
from .repos import RemoteRepo
from .utils import ask_for, create_hash
from .project import Project
from .tag import Tags
from .site import reload_static, export_index
from .gui import select
from .command import rsync


ORG_PROJECT_READ = os.path.abspath("/home/laurent/Org/projets.org")
# PROJECT_ROOT = Path("/home/laurent/Projets").absolute()


class Moum:
    """Moum main class"""
    def __init__(self, root=None, project_json=None, config=None):
        self.root = root
        if not self.root:
            self.root = MOUM_ROOT
        if not config:
            config = os.path.join(self.root, 'config.json')
        self.cache_path = os.path.join(self.root, 'cache')
        self.cache_file = os.path.join(self.cache_path, 'projects_cache.json')

        self.data = {"repos": [], "projects": []}
        self.projects = []
        self.projects_by_name = {}
        self.repos = []
        self.posts = []
        self.posts_sorted = []
        self.posts_links = []
        self.posts_tags = []
        self.tags = Tags(self)

        self.menu = {'fr': [], 'en': []}

        self.check_moum_dir()

        self.config = JsonDict(config)

        self.project_json = project_json
        if not self.project_json:
            self.project_json = os.path.join(self.root, 'projects.json')
        self.load_from_json(self.project_json)

        self.projects_list = self.data["projects"]
        self.load_cache()

        if 'project_root' in self.config:
            self.project_root = Path(self.config['project_root']).absolute()
            self.check_project_folder()


        self.repos_by_name = {}
        for i in self.repos:
            self.repos_by_name[i["name"]] = i

        self.project = None
        self.repo_data = None
        self.repo = None
        self.remote = None

        self.publish_export = False
        self.tmp_export = None

        if 'active_project' in self.config:
            self.active_project = self.config['active_project']
            if self.active_project in self.projects_by_name:
                print('Activate project:', self.active_project)
                if self.active_project not in ['None', 'none', '']:
                    if self.active_project in self.projects_by_name:
                        self.activate_project(self.active_project)
            else:
                self.config['active_project'] = 'None'

    def get_site_export(self):
        """get site export path"""
        if self.tmp_export:
            return self.tmp_export
        if self.publish_export:
            return self.config['site_export_publish']
        return self.config['site_export']

    def get_site_root(self):
        """get site url root"""
        if self.publish_export:
            return self.config['site_root_publish']
        return self.config['site_root']

    def load_cache(self):
        """load project data from cache"""
        if not os.path.isfile(self.cache_file):
            self.import_projects_from_moum_files()

    def import_projects_from_moum_files(self):
        """guild or build cache data"""
        self.projects = []
        self.repos = []
        self.posts = []
        self.posts_links = []
        self.posts_tags = []
        self.projects_by_name = {}
        for project_data in self.projects_list:
            project = Project.load(project_data['path'])
            if project:
                project.set_moum(self)
                self.projects_by_name[project.name] = project
                self.projects.append(project)
                self.repos += project.get_repos()
                self.posts += project.get_posts()

        self.get_child_projects()

        self.posts_sorted = sorted(self.posts)
        for post in self.posts:
            post.set_moum(self)
            for tag in post.tags:
                if tag not in self.posts_tags:
                    self.posts_tags.append(tag)
            self.posts_links.append(post.link)
        self.load_menu()
        self.tags.update()

    def get_child_projects(self):
        """add child project to parent project on loading"""
        for project in self.projects:
            if project.parent_project:
                parent_name = project.parent_project
                if parent_name in self.projects_by_name:
                    parent = self.projects_by_name[parent_name]
                    parent.add_child_project(project)
                    project.get_parent()
                else:
                    print('Parent project does not exist', parent_name)

    def check_project_folder(self):
        """check if every folder in the Projects folder is a project"""
        all_projects = [
            p.name
            for p in self.projects
        ]
        not_in_projects = []
        for folder in self.project_root.iterdir():
            if folder.name not in all_projects:
                print('Not a project: ', folder)
                not_in_projects.append(folder.name)
        return not_in_projects

    def load_menu(self):
        """load menu from json if menu.json exists"""
        menu_path = Path(self.root) / 'menu.json'
        if menu_path.exists():
            with open(menu_path, encoding="utf-8") as menu_file:
                self.menu = json.load(menu_file)

    def load_from_json(self, project_json):
        """load project list from json file"""
        if os.path.exists(project_json):
            with open(project_json, 'r', encoding="utf-8") as project_file:
                self.data = json.load(project_file)
        else:
            self.data = {"repos": [], "projects": []}

    def check_moum_dir(self):
        """create moum data folder if necessary"""
        if not os.path.isdir(self.root):
            path = Path(self.cache_path)
            path.mkdir(parents=True)

    def save(self):
        """save data to json"""
        with open(self.project_json, "w", encoding="utf-8") as project_file:
            json.dump(
                self.data,
                project_file,
                indent=2,
                ensure_ascii=False
            )

    def clear(self):
        """clear all the data"""
        self.data = {"repos": [], "projects": []}
        self.repos = self.data["repos"]
        self.projects_list = self.data["projects"]
        self.save()

    def activate_project(self, project_name=None, persistent=True):
        """activate a project from name"""
        if not project_name:
            project_name = ask_for([i["name"] for i in self.projects_list])
        self.project = self.projects_by_name[project_name]
        # if len(self.project["repos"]) == 1:
        #     self.activate_repo(self.project["repos"][0])
        # elif len(self.project["repos"]) > 1:
        #     self.activate_repo()

        if persistent:
            self.config['active_project'] = project_name

    def activate(self, path):
        """activate a project frmo path"""
        abs_path = Path(path).absolute()
        if str(self.project_root) in str(abs_path):
            for project in self.projects:
                if project.path in str(abs_path):
                    return project
        return None

    def add_project(self, path):
        """add a project from path"""
        project_path = Path(path)
        if project_path.is_dir():
            name = project_path.name

            new_project = {"name": name,
                           "path": path
                           }
            self.projects_list.append(new_project)
            self.projects_by_name[name] = new_project
            self.save()
        else:
            print('not found', path)

    def add_project_data(self, new_project):
        """
        add project data to projects list

        used by import script from dirs
        """
        new_project.find_repos()
        new_project.save()
        if new_project.name not in self.projects_by_name:
            project_entry = new_project.export_to_list()
            self.projects_list.append(project_entry)
            self.projects_by_name[new_project.name] = project_entry
            self.save()

    def create_project(self, name):
        """create a new project"""
        path = self.project_root / name
        data = {}
        data['uid'] = create_hash()
        data['name'] = name
        data['path'] = str(path)
        new_project = Project(**data)
        new_project.save()
        self.add_project_data(new_project)
        return new_project

    def create_project_from_project_folder(self):
        """
        init project for all folder in PROJECT_ROOT
        that are not a project
        """
        new_projects = self.check_project_folder()
        for project_name in new_projects:
            self.create_project(project_name)

    # repo (WIP)
    def add_repo(self, repo_path, project=None):
        """add a repo to repo list"""
        lst_repo_path = [i["local_path"] for i in self.repos]
        local_path = os.path.abspath(repo_path)
        if os.path.exists(repo_path) and local_path not in lst_repo_path:
            repo_data = get_repo_data(repo_path)
            repo_data["local_path"] = local_path
            remote = RemoteRepo(repo_data, self.config, self)
            repo_data["gitlab_id"] = remote.get_gitlab_id()
            self.repos.append(repo_data)
            self.repos_by_name[repo_data["name"]] = repo_data
        if project:
            self.projects_by_name[project]['repos'].append(repo_data["name"])
        self.save()

    def activate_repo(self, repo_name=None):
        """activate a repo"""
        if not repo_name:
            repo_name = ask_for([i["name"] for i in self.repos])
            self.activate_repo(repo_name)
        self.repo_data = self.repos_by_name[repo_name]
        self.repo = RemoteRepo(self.repo_data, self.config, self)
        self.remote = self.repo.remote

    def print_issues(self):
        """print issues of the current repo"""
        self.repo.print_issues()

    # tmp / tests
    def filter(self, contains, category='project'):
        """
        dumb filtering projects
        """
        filtered = []
        if category == 'project':
            filtered = filter_items(self.projects, contains)
        elif category == 'post':
            filtered = filter_items(self.posts, contains)
        return filtered

    def run(self):
        """run script for project"""
        projects = [
            project for project in self.projects
            if project.run
        ]

        project = ask_for(projects)
        project.run_script()

    def build(self, deep=True, path=None):
        """build the site"""
        # export_site(self, None, deep)
        print('export site, deep:', deep)
        if not path:
            path = self.get_site_export()
        output = Path(path)
        output_en = output / 'en'
        if not output_en.exists():
            output_en.mkdir(parents=True)

        # copy static files
        reload_static(path)

        # export posts
        posts = sorted([p for p in self.posts if p.status == 'published'])
        posts.reverse()
        for post in posts:
            post.export_html(deep)

        # export index
        export_index(self, posts, path)

        # export tags
        self.tags.export_html()

    def refresh(self):
        """refresh html content"""
        self.build(False)

    def publish(self):
        """publish html content"""
        self.publish_export = True
        self.build()
        # src = self.config['site_export_publish'] + '/'
        # if 'site_dst_publish' in self.config:
        #     dst = self.config['site_dst_publish']
        #     rsync(src, dst)

    def shortcut_posts(self):
        """create link to all posts in Moum data folder"""
        posts = self.posts_sorted[::-1]
        posts_root = Path(self.root) / 'posts'
        if not posts_root.exists():
            posts_root.mkdir()
        for i, post in enumerate(posts):
            link = posts_root / f'{ i:%02 }_{post.ling}'
            data_dir = Path(post.path).absolute()
            link.symlink_to(data_dir, True)

    def select_post(self):
        """interactively select one or several posts"""
        return select(self.posts, filter_items)

    def select(self):
        """interactively select one or several elements"""
        return select([*self.posts, *self.projects], filter_items)


def filter_items(items, contains):
    """
    filter -items- list if -contains- is in one
    of all_attrs attributes
    """
    all_attrs = ['tags', 'project_tags', 'name', 'description', 'label']
    filtered = []
    for item in items:
        attrs_str = ''
        for attr in all_attrs:
            if attr in item.__dict__:
                attrs_str += str(item.__dict__[attr])
            attrs_str = attrs_str.lower()
        if contains.lower() in attrs_str:
            filtered.append(item)
    return filtered


if __name__ == "__main__":
    moum = Moum()
