#!/usr/bin/python
# coding: utf-8

"""parsing orgfile"""

import os
import string
import random
import re


def check_if_dir_in_project_path(project_nodes, root):
    """
    for each node of a list of project nodes,
    check if the project exist in -root- folder
    """

    projects_in_path = [i.lower() for i in os.listdir(root)]
    is_in_path = []
    for project in project_nodes:
        if project.heading.lower() not in projects_in_path:
            print(project.heading)
        else:
            is_in_path.append(project.heading)
    print()
    print('ok')
    for i in is_in_path:
        print(i)


def parse_propertie(line):
    """
    parse a line of property from the properties section
    of an org node
    return (None, None) if the property is not well formed
    """
    if line.strip()[0] != ":":
        return None, None
    line = line.strip()[1:]
    pos = line.find(":")
    if pos < 0:
        return None, None
    prop_name = line[:pos]
    prop_value = line[(pos+1):].strip()
    return prop_name, prop_value


def parse_tags(heading):
    """
    Return tags as a list
    or None if no tags or tags are not well formed
    """
    if heading:
        last_exp = heading.split()[-1].strip()
        if last_exp.startswith(":") and last_exp.endswith(":"):
            return last_exp[1:-1].split(":")
    return None


def parse_todo(heading, lst_todo=None):
    """parse todo from heading"""
    if heading:
        todo = heading.split()

        if len(todo) > 1:
            todo = todo[1]
        else:
            return None

        if lst_todo is None and todo.isupper():
            return todo

        if lst_todo and todo in lst_todo:
            return todo

    return None


def parse_orgfile_heading(lines):
    """return a dic containing file heading properties"""
    properties = {}
    for line in lines:
        if line.startswith('#+'):
            line = line[2:].strip()
            pos = line.find(':')
            if pos > 0:
                propertie_name = line[:pos].strip()
                propertie_value = line[pos+1:].strip()
            else:
                propertie_name = line
                propertie_value = None
            properties[propertie_name] = propertie_value
    return properties


def create_id():
    """create org id"""
    new_id = ""
    for i in range(5*4):
        for j in range(4):
            new_id += random.choice(string.ascii_letters+"0123456789")
            new_id += "-"
    return new_id[:-1]


class OrgNode:
    """OrgNoe class"""
    def __init__(self, level, parent=None, heading=None):
        self.children = []
        self.child_by_id = {}
        self.level = level
        self.heading = heading
        self.heading_modified = False
        self.parent = parent
        self.lines = []
        self.todo = None
        self.title = ''
        self.tags = []
        self.properties = {}
        self.properties_pos = [0, 0]
        self.properties_modified = False

    def add_child(self, child):
        """add child node and add to child_by_id"""
        self.children.append(child)
        if "ID" in child.properties:
            self.child_by_id[child.properties["ID"]] = child

    def get_properties(self):
        """parse properties"""
        in_properties_section = False
        for i, line in enumerate(self.lines):
            if ":PROPERTIES:" in line:
                in_properties_section = True
                self.properties_pos[0] = i
            if in_properties_section:
                if ":END:" in line:
                    in_properties_section = False
                    self.properties_pos[1] = i
                    break
                prop_name, prop_value = parse_propertie(line)
                if prop_name:
                    self.properties[prop_name] = prop_value

    def get_tags(self):
        """get tags from heading"""
        self.tags = parse_tags(self.heading)

    def get_todo(self, lst_todo=None):
        """get todos from heading"""
        self.todo = parse_todo(self.heading, lst_todo=lst_todo)

    def get_title(self):
        """
        get title from heading
        (remove todos and tags)
        """
        self.title = ''
        if self.heading:
            pos = self.heading.find(' ')
            if self.todo:
                pos += len(self.todo) + 1

            len_tags = 0
            if self.tags:
                len_tags = len(':'.join(self.tags))+2

            self.title = self.heading.strip()[pos:-len_tags].strip()

    def get_meta(self, lst_todo=None):
        """get all metadata"""
        self.get_tags()
        self.get_properties()
        self.get_todo(lst_todo=lst_todo)
        self.get_title()

    def set_tag(self, tag):
        """add a tag or a list of tags"""
        self.heading_modified = True
        if isinstance(tag, str):
            self.tags.append(tag)
        elif isinstance(tag, list):
            self.tags += tag

    def set_propertie(self, propertie_name, propertie_value):
        """set propertie value"""
        self.properties_modified = True
        self.properties[propertie_name] = propertie_value

    def format_properties(self):
        """format properties in org format"""
        dec = ' '*(self.level+1)
        txt = dec + ":PROPERTIES:\n"
        for propertie_name, propertie_value in self.properties.items():
            txt += dec + f':{ propertie_name }: { propertie_value}\n'
        txt += dec + ":END:\n"

        return txt

    def create_heading(self):
        """recreate the heading from data"""
        txt = "*" * self.level
        if self.todo:
            txt += " " + self.todo
        txt += " " + self.title
        tags = ""
        if self.tags:
            tags = ":".join([''] + self.tags + [''])
        spaces = 77 - len(txt) - len(tags)
        if spaces > 0:
            txt += ' ' * spaces
        txt += tags
        txt += '\n'

        self.heading = txt

    def __str__(self):
        txt = ""
        if self.heading_modified:
            self.create_heading()

        if self.heading:
            txt += self.heading

        start_line = 0
        if self.properties_modified:
            for line in self.lines[:self.properties_pos[0]]:
                txt += line
            txt += self.format_properties()
            start_line = self.properties_pos[1]+1

        for line in self.lines[start_line:]:
            txt += line
        return txt


class OrgFile:
    """My own OrgFile class to sync with gitlab issues"""
    def __init__(self, filename):
        self.filename = filename
        self.heading = {"SEQ_TODO": "TODO(t) OPENED(o) | DONE(d) CLOSED(c)"}
        self.root = OrgNode(0)
        self.nodes = []
        self.nodes_by_id = {}
        self.todos = []
        self.out = ''

    def create(self, issues):
        """create heading with issues"""
        txt = self.create_heading()
        txt += self.write_issues(issues)
        with open(self.filename, "w", encoding="utf-8") as f:
            f.write(txt)

    def create_heading(self):
        """write all headings in top of the file"""
        txt = ""
        for k, i in self.heading.items():
            txt += f'#+{ k }: {i}\n'
        txt += "\n"

        return txt

    def get_todos(self):
        """get all todos in page heading"""
        if "SEQ_TODO" in self.heading:
            self.todos = re.sub(r'\(.\)|\|', '', self.heading['SEQ_TODO'])
            self.todos = self.todos.split()

    def check_if_id(self, issues):
        """create id if not exists"""
        for issue in issues:
            if "org_id" not in issue:
                issue["org_id"] = create_id()

    def update_issues(self, issues, parent_id):
        """update issues for parent id"""
        self.check_if_id(issues)
        if parent_id in self.nodes_by_id:
            parent = self.nodes_by_id[parent_id]
            for issue in issues:
                if issue["org_id"] in parent.child_by_id:
                    issue_node = parent.child_by_id[issue["org_id"]]
                    self.update_issue(issue, issue_node)
                else:
                    self.add_issue(issue, parent)

    def update_issue(self, issue, issue_node):
        """update issue WIP?"""
        print("update", issue, issue_node)

    def add_issue(self, issue, parent):
        """add issue to parent"""
        heading = "*" * (parent.level+1)
        heading += " %s #%s %s\n" % (issue["state"].upper(),
                                     issue["id"],
                                     issue["title"])
        o = OrgNode(parent.level+1, parent, heading)
        self.add_node(o, parent)

    def write_issues(self, issues):
        """write all issues"""
        txt = ""
        for issue in issues.values():
            txt += self.write_issue(issue)

        return txt

    def write_issue(self, issue):
        """write issue in orgmode node"""
        txt = f'* { issue["state"].upper() }'
        txt += f' #{ issue["id"] }'
        txt += f' {issue["title"] }\n'
        if issue["description"]:
            txt += issue["description"] + "\n"
        return txt

    def add_node(self, node, parent):
        """
         add node to tree:
        - add to parent
        - add to nodes list
        - add to nodes_by_id dictionnary
        """
        # node.get_meta()
        parent.add_child(node)
        self.nodes.append(node)
        if "ID" in node.properties:
            self.nodes_by_id[node.properties["ID"]] = node

    def parse(self):
        """
        get each nodes and add them to the root node
        + parse properties
        + create relational dictionnary: self.nodes_by_id
        """
        current_level = 0
        current_node = self.root
        last_node_at_level = {0: self.root}
        self.parse_heading()
        self.get_todos()
        with open(self.filename, "r", encoding='utf-8') as f:
            lines = f.readlines()
            for line in lines:
                if line.startswith("*"):
                    level = len(line.split()[0])
                    current_node.get_meta(self.todos)
                    current_parent = self.root
                    if level == current_level:
                        # create node same level
                        new_node = OrgNode(level, current_node.parent, line)
                        current_parent = current_node.parent
                    elif level > current_level:
                        # create chlidren
                        new_node = OrgNode(level, current_node, line)
                        current_parent = current_node
                    elif level < current_level:
                        # create parent node
                        sister = last_node_at_level[level]
                        new_node = OrgNode(level, sister.parent, line)
                        current_parent = sister.parent
                    self.add_node(new_node, current_parent)
                    current_level = level
                    current_node = new_node
                    last_node_at_level[current_level] = current_node
                else:
                    current_node.lines.append(line)

        self.get_nodes_by_id(self.root)

    def parse_heading(self):
        """Parse top file heading"""
        head = []
        with open(self.filename, "r", encoding='utf-8') as f:
            lines = f.readlines()
            for line in lines:
                if line.startswith('*'):
                    break
                head.append(line)
        self.heading = parse_orgfile_heading(head)

    def get_nodes_by_id(self, node):
        """
        Recursively add node reference in self.nodes_by_id
        """
        if 'ID' in node.properties:
            self.nodes_by_id[node.properties["ID"]] = node
        else:
            print(f'no ID in: { node.heading }')

        for children in node.children:
            self.get_nodes_by_id(children)

    def write_recursively(self, node):
        """write the current note and his children"""
        self.out += str(node)
        for c in node.children:
            self.write_recursively(c)

    def write(self, output_filename):
        """write orgfile to output_filename"""
        self.out = ""
        self.write_recursively(self.root)

        with open(output_filename, "w", encoding="utf-8") as f:
            f.write(self.out)
