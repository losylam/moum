# coding: utf-8

"""
Tag class for Moum
"""

import json

from dataclasses import dataclass
from pathlib import Path

from moum.utils import create_hash
from moum.schema import ElemData
from moum.site import get_post_data, export_template


class Tags(dict):
    """ Tag dataclass for Moum"""
    def __init__(self, moum=None):
        super().__init__()
        self._moum = None
        self._path = ''
        if moum:
            self.set_moum(moum)

    def __setitem__(self, key, value):
        """set item if tag not exists"""
        if key in self:
            print('tag exists:', key)
        else:
            super().__setitem__(key, value)

    def set_moum(self, moum):
        """init moum"""
        self._moum = moum
        self._path = Path(self._moum.root, 'tags.json')
        self.load(self._path)
        if len(self._moum.projects) > 0:
            self.update()

    def load(self, path):
        """load from path"""
        if path and not path.is_file():
            return
        with open(self._path, encoding="utf-8") as f:
            tags_json = json.load(f)
        for tag in tags_json:
            self.append(tag)

    def to_json(self, path):
        """save to path as json list"""
        tags_json = [tag.serialize() for tag in self.values()]
        with open(path, 'w', encoding="utf-8") as f:
            json.dump(tags_json, f, indent=2, ensure_ascii=False)

    def save(self, path=None):
        """save as json list"""
        if not self._path and not path:
            print('no path, cannot save')
            return False
        if not path:
            path = self._path
        self.to_json(path)

        return True

    def append(self, item):
        """append a tag"""
        if isinstance(item, str):
            self.append_str(item)
        elif isinstance(item, Tag):
            self[item.name] = item
        elif isinstance(item, dict):
            self.append_dict(item)

    def append_str(self, name):
        """create a new tag from name"""
        new_tag = Tag(uid=create_hash(), name=name)
        self[name] = new_tag

    def append_dict(self, item):
        """create a new tag from dict"""
        if 'uid' not in item:
            item['uid'] = create_hash()
        tag = Tag(**item)
        self[tag.name] = tag

    def get_tag_data(self, tag_name, lang=None):
        """get serialized tag data"""
        if tag_name in self:
            tag = self[tag_name]
        else:
            tag = Tag.create(tag_name)

        data = tag.serialize()
        data['link'] = '/tag/' + tag.label

        if lang == 'en':
            data['link'] = '/en/tag/' + tag.label_en
            data['label'] = tag.label_en
        return data

    def update(self):
        """update tag list from Moum data"""
        if not self._moum:
            return False

        for post in self._moum.posts:
            for tag in post.tags:
                if tag not in self:
                    self.append(tag)
                self[tag].append(post)

        for project in self._moum.projects:
            for tag in project.tags:
                if tag not in self:
                    self.append(tag)
                self[tag].append(project)
        for tag in self.values():
            tag.set_moum(self._moum)
        return True

    def export_html(self):
        """export tag as html page"""
        if not self._moum:
            return
        for tag in self.values():
            export_en = tag.export_html('en')
            tag.export_html('fr', export_en)


@dataclass(repr=False)
class Tag(ElemData):
    """ Tag dataclass """
    category: str = 'tag'
    label_en: str = ''
    description_en: str = ''

    def __post_init__(self):
        self._moum = None
        self._tagged = []
        if not self.label:
            self.label = self.name
        if not self.label_en:
            self.label_en = self.label

    def set_moum(self, moum):
        """link to main moum"""
        self._moum = moum

    @staticmethod
    def create(name):
        """Create a new tag from name"""
        uid = create_hash()
        return Tag(uid, name)

    def append(self, item):
        """append a tagged item to the list"""
        self._tagged.append(item)

    def posts(self, lang=None):
        """retur all tagged posts"""
        posts = [
            item
            for item in self._tagged
            if item.category == 'post'
        ]
        if lang == 'en':
            posts = [
                post
                for post in posts
                if post.title_en
            ]
        posts = sorted(posts)
        posts.reverse()
        return posts

    def export_html(self, lang=None, en_exists=False):
        """
        export tag page in -lang-
        add the english full link in -en_exists-
        """
        posts = self.posts(lang)
        if len(posts) == 0:
            return False
        items = get_post_data(posts, lang)
        root = self._moum.get_site_root()
        link = f'tag/{ self.label }'
        link_en = f'en/tag/{ self.label_en}'

        data = {
            'title': f'Tag: { self.label }',
            'template': "tag",
            'root': root,
            'lang': 'fr',
            'link': link,
            'menu': self._moum.menu['fr'],
            'fulllink_fr': root + '/' + link,
            'description': self.description
        }

        if en_exists or lang == 'en':
            data['fulllink_en'] = root + '/' + link_en

        if lang == 'en':
            data['title'] = f'Tag: { self.label_en }'
            data['link'] = link_en
            data['menu'] = self._moum.menu['en']
            data['lang'] = 'en'
            data['description'] = self.description_en

        path = Path(
            self._moum.get_site_export(),
            data['link'],
            'index.html')

        if not path.parent.exists():
            path.parent.mkdir(parents=True)

        export_template('tag.html', {
            'data': data,
            'items': items
        }, path)
        return True
