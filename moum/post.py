#!/usr/bin/python
# coding: utf-8

"""
Post class for Moum
"""

import json
import shutil

from pathlib import Path
from dataclasses import dataclass, field
from typing import Literal

from moum.utils import create_hash, time_to_json, ask_for_several
from moum.utils import normalize_string, local_date
from moum.schema import ElemData
from moum.command import edit, navigate, im_resize
from moum.site import get_html_body, gfm_to_html, export_template

TEMPLATES = [
    'gallery',
    'app',
    'blank',
    'video'
]


@dataclass(repr=False)
class Post(ElemData):
    """ Post dataclass """
    category: str = 'post'
    path: str = ''
    link: str = ''
    link_en: str = ''
    title: str = ''
    title_en: str = ''
    description: str = ''
    description_en: str = ''
    project: str = ''
    post_date: str = ''
    post_type: Literal['post', 'page', 'project_post'] = 'post'
    template: str = ''
    thumbnail: str = ''
    status: Literal['published', 'draft'] = 'draft'
    tags: list = field(default_factory=list)
    frontpage: bool = True
    gallery: str = ''
    video: str = ''
    css: list = field(default_factory=list)
    lib: list = field(default_factory=list)
    script: list = field(default_factory=list)
    assets: str = ''

    def __post_init__(self):
        self._moum = None
        self._gallery_images = []
        self._template_file = "post.html"

        self._data_dir = Path(self.path).absolute()
        self._data_path = self._data_dir / "moum-post.json"
        if not self._data_path.exists():
            print('post not found', self.path)

        if not self.link:
            self.link = self.name

        if self.thumbnail:
            self._thumbnail_link = "/" / Path(self.link) / self.thumbnail

        if self.template:
            self._template_file = f'post-{ self.template }.html'

    def __lt__(self, post):
        return self.post_date <= post.post_date

    def _site_export(self):
        return Path(self._moum.get_site_export()).absolute()

    def _path_export(self):
        site_export = self._site_export()
        return site_export / self.link

    def set_moum(self, moum):
        """link to main moum"""
        self._moum = moum
        self._project = None
        if self.project and self.project in self._moum.projects_by_name:
            self._project = self._moum.projects_by_name[self.project]

    def to_json(self, path):
        """export static data to json"""
        data = self.serialize()
        with open(path, 'w', encoding='utf-8') as stream:
            json.dump(data, stream, indent=4, ensure_ascii=False)

    def save(self):
        """save to moum folder"""
        self.to_json(self._data_path)

    @staticmethod
    def from_json(path):
        """load from json"""
        with open(path, 'r', encoding="utf-8") as stream:
            data = json.load(stream)
        return Post(**data)

    @staticmethod
    def load(path):
        """load from moum folder"""
        json_path = Path(path, 'moum-post.json')
        return Post.from_json(json_path)

    def create(self):
        """Create the post folder and files"""
        Path(self.path).mkdir(parents=True)
        if self.gallery:
            Path(self.path, self.gallery).mkdir(parents=True)
        content_path = Path(self.path, 'post.md').absolute()
        if not content_path.exists():
            content_path.touch()
        self.save()

    def get_content(self, lang=None):
        """
        Get post content

        read and parse markdown file
        or extract body from html file
        """
        base_path = self.path + '/post'
        if lang:
            base_path += '_en'
        content_path = Path(base_path + '.md')

        if content_path.is_file():
            content = content_path.read_text(encoding='utf-8')
            return gfm_to_html(content)

        content_path = Path(base_path + '.html')
        if content_path.is_file():
            content = content_path.read_text(encoding='utf-8')
            return get_html_body(content)
        print('content not found: ', content_path)
        return ''

    def edit_content(self):
        """Edit post content"""
        content_path = Path(self.path, 'post.md')
        edit(content_path)

    def navigate(self, app='emacs'):
        """open in a file explorer"""
        navigate(self.path, app)

    def publish(self, pub=True):
        """change status to 'published'"""
        if pub:
            self.status = 'published'
        else:
            self.status = 'draft'
        self.save()

    def export_thumbnail(self, deep):
        """
        Export thumbnail

        - get path/filename
        - resize and copy if deep=True
        """
        pin = self._data_dir / self.thumbnail
        pout = self._path_export() / self.thumbnail

        if not pin.exists():
            print('no thumbnail')
            return False

        if deep and not pout.parent.exists():
            Path(pout.parent).mkdir(parents=True)
        if deep:
            im_resize(pin, pout, 640)
        return True

    def export_gallery(self, deep=True):
        """
        Export Gallery

        - get the images path
        - resize and copy images if deep=True
        """
        pin = self._data_dir / self.gallery
        pout = self._path_export() / self.gallery

        if not pin.exists():
            print('gallery not found:', str(pin))
            return

        images = [
            i for i in pin.iterdir()
            if i.suffix.lower() in ['.jpg', '.jpeg', '.png', '.gif']
        ]
        images.sort()

        if deep or not pout.exists():
            pout.mkdir(parents=True)

        for pin_i in images:
            pout_i = pout / pin_i.name
            if deep:
                export_image(pin_i, pout_i)

        self._gallery_images = images

    def export_statics(self, deep):
        """Copy static files"""
        statics = [*self.css, *self.script]
        for path in statics:
            pin = self._data_dir / path
            pout = self._path_export() / path
            if not pout.parent.exists():
                pout.parent.mkdir(parents=True)
            shutil.copy(pin, pout)

        if deep:
            for path in self.lib:
                pin = self._data_dir / path
                pout = self._path_export() / path
                if not pout.parent.exists():
                    pout.parent.mkdir(parents=True)
                shutil.copy(pin, pout)

        if self.assets:
            path = Path(self.assets)
            pin = self._data_dir / path
            pout = self._path_export() / path
            if pout.exists():
                shutil.rmtree(pout)
            shutil.copytree(pin, pout)

    def get_links(self, data):
        """get static file links relative to the site root"""
        root = self._moum.get_site_root()
        # print(root + '/' + self.link)
        for name in ['lib', 'css', 'script']:
            data[name] = [
                root + '/' + self.link + '/' + i
                for i in data[name]
            ]
        return data

    def get_tags(self, lang=None):
        """get all tags data from moum.tags"""
        tags = self.tags
        if self._moum:
            tags = [self._moum.tags.get_tag_data(tag_name, lang)
                    for tag_name in self.tags]
        return tags

    def get_parent(self, lang='fr'):
        """
        get parent post and link
        """
        if self._project:
            parent_post = self._project.get_main_post()
            if parent_post and parent_post != self:
                return parent_post.get_data(lang)

    def get_children(self, lang='fr'):
        """
        if the post is the main page of a project
        return all post from the project and all post
        from other children projects
        """
        if self.post_type == 'project_post' and self._project:
            children_posts = self._project.get_children_posts()
            children_posts.reverse()

            out = []
            for post in children_posts:
                if post != self and post.title_en:
                    out.append(post.get_data(lang))
            return out
        return []

    def get_data(self, lang='fr'):
        """Create data used by the template"""
        data = self.serialize()
        if 'post' in self.post_type:
            data['local_date'] = local_date(self.post_date)
        data['root'] = self._moum.get_site_root()

        # thumbnail, gallery and staic links
        # are the same for translated pages
        frontmedia = []
        if self.gallery:
            for image in self._gallery_images:
                src = Path('/', self.link, self.gallery, image.name)
                frontmedia.append({'src': str(src)})
        data['frontmedia'] = frontmedia

        if self.thumbnail:
            thumbnail_link = '/' / Path(self.link) / self.thumbnail
            data['thumbnail_link'] = str(thumbnail_link)

        if self.link_en:
            root = self._moum.get_site_root()
            data['fulllink_en'] = root + '/en/' + self.link_en
            data['fulllink_fr'] = root + '/' + self.link

        data['lang'] = lang
        data['tags'] = self.get_tags(lang)
        data['menu'] = self._moum.menu[lang]
        data['parent'] = self.get_parent(lang)

        if lang == 'en':
            data['link'] = 'en/' + data['link_en']
            data['description'] = data['description_en']
            data['title'] = data['title_en']

        data = self.get_links(data)

        return data

    def export_assets(self, deep=True):
        """Create post folder, copy assets and return data"""
        path_export = self._path_export()
        if deep and path_export.exists():
            shutil.rmtree(path_export)
            path_export.mkdir(parents=True)
        if not path_export.exists():
            path_export.mkdir(parents=True)

        if self.thumbnail:
            self.export_thumbnail(deep)
        if self.gallery:
            self.export_gallery(deep)
        self.export_statics(deep)

    def export_html(self, deep=True):
        """
        Export post to html

        if self._title_en exists, export also in english
        """

        self.export_assets(deep)

        data = self.get_data()
        data['children'] = self.get_children()

        content = self.get_content()
        path = self._path_export() / 'index.html'

        export_template(self._template_file, {
            "data": data,
            "content": content
        }, path)

        if self.title_en:
            self.export_html_en()

    def export_html_en(self):
        """Export to html the post in english"""

        data_en = self.get_data('en')
        data_en['children'] = self.get_children('en')

        content = self.get_content('en')
        path_en = self._site_export() / data_en['link'] / 'index.html'

        if not path_en.parent.exists():
            path_en.parent.mkdir(parents=True)

        # copy assets to translated pages
        if self.assets:
            path = Path(self.assets)
            pin = self._data_dir / path
            pout = Path(self._site_export(), 'en', self.link_en, path)
            if pout.exists():
                shutil.rmtree(pout)
            shutil.copytree(pin, pout)

        export_template(self._template_file, {
            "data": data_en,
            "content": content
        }, path_en)


def ask_link(title, moum):
    """
    Ask for a link while creating new post
    """
    link = ""
    tmp_link = normalize_string(title)
    ok_link = input(f'link ({ tmp_link }) ')
    if ok_link == '':
        link = tmp_link
    else:
        link = ok_link
    while link in moum.posts_links:
        print('[warning] link exists')
        link = input('link : ')
    return link


def create_post(project, moum):
    """
    Create a new post for -project-
    interactively
    """
    data = {}
    data['uid'] = create_hash()
    data['title'] = input("name : ")
    data['link'] = ask_link(data['title'], moum)
    data['post_date'] = time_to_json()
    data['name'] = data['link']
    data['description'] = input("description : ")
    if input("tags : (yes) ") == '':
        tag_list, data['tags'] = ask_for_several(moum.posts_tags, 'tags :')
    data['project'] = project.name

    abs_path = Path(project.path).absolute() / "moum/posts" / data['link']
    data['path'] = str(abs_path)

    if input('gallery : (yes) ') == '':
        data['gallery'] = './medias/gallery'
        data['template'] = 'gallery'

    post = Post(**data)
    print('New post')
    print(post)
    if not input(f'is about to be created in { post.path } (yes)'):
        post.create()
    return post


def export_image(pin, pout):
    """resize if not gif and copy the image to -pout-"""
    if pin.suffix != '.gif':
        im_resize(pin, pout, 1200)
    else:
        shutil.copy(pin, pout)


def find_posts(path):
    """find all posts path in -path- directory"""
    return [p.parent for p in Path(path).rglob('*/moum-post.json')]


def get_posts(path):
    """get all posts in -path- as Post object"""
    return [Post.load(p) for p in find_posts(path)]
