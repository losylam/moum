"""
Gui elements for Moum
"""

from blessed import Terminal


def filter_txt(elems, text):
    """filter if contains text"""
    return [f for f in elems if text in f]


def filter_start(elems, text):
    """filter if starts with text"""
    return [f for f in elems if f.startswith(text)]


def select(elems, filter_f=None):
    """interactively select elements from elems"""
    filtered = []
    text = ''
    pos = 0
    term = Terminal()

    if filter_f is None:
        filter_f = filter_txt

    def render():
        print(term.clear + term.home)
        print(' >', text)
        print()
        title = f"::: filtered ({len(filtered)} / {len(elems)}) pos { pos }"
        while len(title) < term.width:
            title += ':'
        print(term.black_on_violet(title))
        posy = term.get_location()[0] + 2
        for i, elem in enumerate(filtered[:(term.height-posy)]):
            if i == pos:
                print(term.violet_on_black(f" - { elem } "))
            else:
                print(f" - { elem } ")


    print(term.home + term.clear)
    with term.cbreak(), term.hidden_cursor():
        filtered = filter_f(elems, text)
        render()
        out = False
        while not out:
            inp = term.inkey()
            if inp.code in [term.KEY_ESCAPE, term.KEY_ENTER]:
                out = True
            elif inp.code in [term.UP, term.KEY_UP]:
                pos -= 1
                if pos < 0:
                    pos = 0
            elif inp.code in [term.DOWN, term.KEY_DOWN]:
                pos += 1
                if pos > len(filtered):
                    pos = len(filtered)
            elif inp.code in [term.KEY_BACKSPACE, term.KEY_DELETE]:
                text = text[:-1]
                pos = 0
            else:
                text += str(inp)
                pos = 0
            filtered = filter_f(elems, text)
            render()
    # print(term.clear)
    if len(filtered) == 1 or pos != 0:
        return filtered[pos]

    return filtered
