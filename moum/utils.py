#!/usr/bin/python
# coding: utf-8

"""Utilities for Moum"""

import random
import time
import unicodedata

from pathlib import Path


def create_hash(length=16):
    """create a simple hash of length -length-"""
    c = '0123456789abcdef'
    return ''.join([random.choice(c) for i in range(length)])


def ask_for(lst):
    """interactively select one element of the list"""
    for i, it in enumerate(lst):
        print(f'[ { i } ]\t{ it }')
    response = input("> ? ")
    return lst[int(response)]


def ask_for_(lst, label=None):
    """
    interactively select on element
    if the response contains a comma, consider it is a list of number
    if the response is text, add the test to the list
    (use by ask_for_several)
    """
    if label:
        print(label, '?')
    for i, it in enumerate(lst):
        print(f"[ { i } ]  { it }")
    response = input("> ? ")
    if response == '' or response.strip() == 'q':
        return False
    if ',' in response:
        response = [lst[int(i)] for i in response.split(',')]
        return lst, response
    if not response.isnumeric():
        lst.append(response.strip())
        return lst, lst[len(lst)-1]
    return lst, lst[int(response)]


def ask_for_several(lst, label=None):
    """
    interactively select several element in the list
    """
    if label:
        print(label, '?')
    lst_num = []
    res = ask_for_(lst)
    while res:
        lst = res[0]
        if isinstance(res[1], list):
            lst_num += res[1]
        else:
            lst_num.append(res[1])
        res = ask_for_(lst)
    return lst, lst_num


def normalize_string(text):
    """normalize string to get slug like string"""
    text = unicodedata.normalize('NFD', text.lower())
    text = text.encode('ascii', 'ignore')
    text = text.decode('utf-8')
    text = text.replace(' ', '-')
    text = text.replace("'", '')
    return text


def yes_or_no(txt):
    """
    ask yes or no

    return true by default
    """
    response = 'm'
    while response not in 'OoyYnN' and response != '':
        response = input(txt + ' [O/n]')

    return response == '' or response in 'OoYy'


def time_struct_from_str(gmt_string):
    """return a time structure from string"""
    return time.strptime(gmt_string, '%Y-%m-%d %H:%M:%S')


def time_to_json(t=None):
    """
    return time as json time string format
    t can be either:
    - None: return present time
    - a time structure
    - a number
    """
    ms = '000'
    if not t:
        t = time.time()
    if isinstance(t, time.struct_time):
        time_struct = t
    elif isinstance(t, str):
        time_struct = time_struct_from_str(t)
    else:
        time_struct = time.gmtime(t)
        ms = str(t).split('.')[1][:3]

    time_str = time.strftime('%Y-%m-%dT%H:%M:%S', time_struct)

    return f'{ time_str }.{ ms }Z'


def json_to_time(t):
    """
    return time structure from json time string
    """
    if not t.endswith('.000Z'):
        t = t[:-4] + '000Z'
    return time.strptime(t, '%Y-%m-%dT%H:%M:%S.000Z')


def local_date(t):
    """
    return date format as DD.MM.YYYY from json string
    """
    try:
        return time.strftime('%d.%m.%Y', json_to_time(t))
    except ValueError:
        pass
    return ''


def get_mtime(path):
    """get mtime from stat"""
    if not isinstance(path, Path):
        path = Path(path)
    return max(i.stat().st_mtime for i in path.glob('**/*'))
