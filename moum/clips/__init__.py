""" Clip data management for Moum """

import json
import os
import shutil
import datetime
import time
import subprocess
from pprint import pprint

from PIL import Image

from moum.utils import ask_for_several
from moum.config import CLIPS_JSON
from moum.clips.peertube_api import send_video, video_api

# video_api = video_api

# JSON_CLIPS = '/home/laurent/Projets/Daily/stats/daily.json'
BASE_DIR = '/home/laurent/Vidéos/Daily/Published'
THUMB_DIR = '/home/laurent/Vidéos/Daily/Thumbs'

PROCESSES = []


class Clips:
    """Clips class for Moum"""
    def __init__(self, filename=CLIPS_JSON):
        self.filename = filename
        if os.path.exists(filename):
            self.load_data(filename)
        else:
            self.data = {
                'tools': [],
                'videos': [],
                'tags': ['geometry', '3d', 'BlackAndWhite'],
                'series': []
            }
        self.tools = self.data['tools']
        self.series = self.data['series']
        self.tags = self.data['tags']
        self.videos = self.data['videos']
        self.hash_meta()
        self.check()

    def __str__(self):
        return json.dumps(self.data, indent=2)

    def hash_meta(self):
        """get all meta as a single string for all videos"""
        self.all_metas = []
        for video in self.videos:
            hash_meta = ''
            for meta in ['tools', 'tags', 'series']:
                for meta_str in video[meta]:
                    hash_meta += meta_str + ' '
            hash_meta += video['title'] + ' '
            hash_meta += video['filename']
            self.all_metas.append(hash_meta)

    def filter(self, meta):
        """filter from meta data"""
        selected = [
            self.videos[i]
            for i in range(len(self.videos))
            if meta in self.all_metas[i]
        ]
        return selected

    def load_data(self, filename):
        """Load data from json"""
        with open(filename, 'r', encoding='utf-8') as stream:
            self.data = json.load(stream)
        for meta in ['series', 'tools', 'tags']:
            self.data[meta] = self.get_meta(meta)

    def get_meta(self, meta):
        """get a set of all values of -meta-"""
        lst = set()
        for video in self.data['videos']:
            lst = lst.union(video[meta])
        lst = list(lst)
        lst.sort()
        return lst

    def save_data(self):
        """save data to json file"""
        with open(self.filename, 'w', encoding="utf-8") as stream:
            json.dump(self.data, stream, indent=2)

    def add_entry(self, filepath, meta=None):
        """add a new video from filepath"""
        if not os.path.exists(filepath):
            print('Le fichier n\'existe pas')
            return

        filename = os.path.split(filepath)[1]

        new_video = {
            'filename': filename,
        }

        if meta is None:
            meta = self.ask_for_meta()
        new_video = {**new_video, **meta}
        self.videos.append(new_video)

    def ask_for_meta(self):
        """interactively ask for each metadata"""
        print('\n===============')
        title = input('titre ? \n')

        print('\n===============')
        self.series, serie_num = ask_for_several(self.series, 'série')

        print('\n===============')
        self.tools, lst_tools = ask_for_several(self.tools, 'outil')

        print('\n===============')
        self.tags, lst_tags = ask_for_several(self.tags, 'tags')

        return {
            'title': title,
            'series': serie_num,
            'tools': lst_tools,
            'tags': lst_tags,
            'ig_pub': '',
            'ig_id': ''
        }

    def add_created(self):
        """add creation date from os.stat"""
        for video in self.videos:
            if 'created' not in video:
                # fp = os.path.join(BASE_DIR, video['filename'])
                # created = date_file(fp)
                created = get_created(video)
                video['created'] = created
                print('add created', video['filename'], created)

    def add_batch(self, directory):
        """add several video that are in -directory-"""
        if not os.path.isdir(directory):
            print(directory, "n'est pas un répertoire")
            return
        meta = self.ask_for_meta()

        filenames = os.listdir(directory)
        for filename in filenames:
            shutil.move(os.path.join(directory, filename), BASE_DIR)
            new_video = {"filename": filename}
            new_video = {**new_video, **meta}
            new_video['created'] = get_created(new_video)
            self.videos.append(new_video)
            print('Vidéo ajoutée : ', filename)

    def create_thumbs(self):
        """crate thumbn"""
        for video in self.videos:
            f_out = video['tube']['uuid'] + '.jpg'
            f_out = os.path.join(THUMB_DIR, f_out)
            if not os.path.exists(f_out):
                get_frame(video, 0)
                print('Miniature créée', video['title'])

    def upload(self, num=0):
        """upload all videos without tube_id"""
        if num == 0:
            num = len([v for v in self.videos if 'tube' not in v])
        cpt = 0
        for video in self.videos:
            if 'tube' not in video:
                print(f'//// send video [{ cpt }/{ num }]')
                pprint(video)
                response = send_video(video)
                print('////', response)
                print()
                self.save_data()
                tube_id = response.video.id
                if cpt >= num:
                    break
                while get_state(tube_id) != 'Published':
                    time.sleep(5)
                cpt += 1

    def check(self):
        """Check if file exist for all clips"""
        is_ok = True
        lst = []
        for video in self.videos:
            filepath = os.path.join(BASE_DIR, video['filename'])
            if not os.path.isfile(filepath):
                lst.append(filepath)
                is_ok = False
        if not is_ok:
            print('Fichiers non trouvés :')
            pprint(lst)
        else:
            print('check ok')


def get_state(tube_id):
    """get video state from peertube api"""
    return video_api.videos_id_get(tube_id).state.label


def complete_title(videos):
    """add to title interactively"""
    for video in videos:
        play(video)
        new_title = input(video['title'] + ' ?')
        video['title'] = video['title'] + ' ' + new_title


def date_file(filepath):
    """get modification data of -filepath-"""
    mtime = os.path.getmtime(filepath)
    mtime_dt = datetime.datetime.fromtimestamp(mtime)
    return mtime_dt.astimezone().replace(microsecond=0).isoformat()


def get_created(video):
    """get created time of video"""
    path = os.path.join(BASE_DIR, video['filename'])
    created = date_file(path)
    return created


def play(video):
    """play the video with vlc"""
    filepath = os.path.join(BASE_DIR, video['filename'])
    com = ['vlc', '--one-instance', filepath]
    with subprocess.Popen(
            com,
            stdout=subprocess.DEVNULL,
            stderr=subprocess.DEVNULL) as process:
        PROCESSES.append(process)
        print('Play:', video['filename'])
        return process
    return None


def get_frame(video, pos):
    """get a frame at -pos-"""
    f_in = os.path.join(BASE_DIR, video['filename'])
    # f_out = video['filename'][:-4] + '.jpg'
    f_out = video['tube']['uuid'] + '.jpg'
    f_out = os.path.join(THUMB_DIR, f_out)
    com = ['ffmpeg', '-i', f_in, '-ss', str(pos), '-vframes', '1', f_out]
    subprocess.call(com)


def get_size(video):
    """get size of a video from thumb size"""
    dimension = None
    if 'tube' in video:
        uuid = video['tube']['uuid']
        image = Image.open(os.path.join(THUMB_DIR, uuid+'.jpg'))
        size = image.size
        dimension = {
            'width': size[0],
            'height': size[1]
        }

        video['size'] = dimension
    return dimension

# bdd.save_data()
# pprint(bdd.data)
# bdd.add_entry('tata')
# pprint(bdd.data)


if __name__ == '__main__':
    clips = Clips(CLIPS_JSON)
