"""Wrapper around peertube api"""

import os
from getpass import getpass
import requests

import peertube
from peertube.rest import ApiException

BASE_DIR = '/home/laurent/Vidéos/Daily/Published'
API_URL = "https://tube.laurent-malys.fr/api/v1"
API_USER = 'lo'
TOKEN_FILE = 'peertube_token'

configuration = peertube.Configuration(
    host=API_URL
    )


def read_token():
    """read token or get a new one"""
    if os.path.isfile(TOKEN_FILE):
        with open(TOKEN_FILE, 'r', encoding="utf-8") as f:
            return f.read().strip()
    else:
        get_token()
        return read_token()


def get_token():
    """get token from peertube api"""
    password = getpass()
    response = requests.get(API_URL + '/oauth-clients/local', timeout=5)
    client = response.json()
    client_id = client['client_id']
    client_secret = client['client_secret']

    data = {
        'client_id': client_id,
        'client_secret': client_secret,
        'grant_type': 'password',
        'response_type': 'code',
        'username': API_USER,
        'password': password
    }

    response = requests.post(API_URL + '/users/token', data=data, timeout=5)
    token_json = response.json()
    print(token_json)
    with open(TOKEN_FILE, 'w', encoding="utf-8") as f:
        f.write(token_json['access_token'])


configuration.access_token = read_token()
api = peertube.ApiClient(configuration)
video_api = peertube.VideoApi(api)
jobs_api = peertube.JobApi(api)


def test_api():
    """test if peertube api is working"""
    config_api = peertube.ConfigApi(api)
    r = config_api.config_get()
    return r


def send_video(video, channel_id=3):
    """send a new video to peertube instance"""
    filepath = os.path.join(BASE_DIR, video['filename'])
    name = video['title']
    # privacy = {'id': 1, 'label': 'Public'}
    privacy = 1
    # category = 4

    created = None
    if 'created' in video:
        created = video['created']

    # tags = [ ('tags[]', 'geometry'), ('tags[]', '3d'), ('tags[]', 'loop') ]

    try:
        response = video_api.videos_upload_post(
            filepath,
            channel_id,
            name,
            privacy=privacy,
            nsfw=False,
            category=4,
            # tags = tags,
            comments_enabled=False,
            download_enabled=False,
            originally_published_at=created,
            wait_transcoding=True
        )

        tubedata = {
            'id': response.video.id,
            'uuid': response.video.uuid,
        }
        video['tube'] = tubedata

        return response

    except ApiException as e:
        print(f'erreur pendant le téléversement: { e }\n')
        return False


def get_video(video):
    """get video data from peertube api"""
    return video_api.videos_id_get(video['tube_id'])

# def get_jobs():
#     return jobs_api.jogs_state_get()
