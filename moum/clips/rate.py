"""rate utility"""

from moum.clips import Clips, play

clips = Clips()


def rate_serie(serie):
    """rate each video of a serie"""
    videos = [v for v in clips.videos if serie in v['series']]
    pos = 0
    again = True
    while again:
        v = videos[pos]
        play(v)
        txt = f"[ { pos } ] { v['title']} ? "
        if 'rating' in v:
            txt += f'({ v["rating"] }) '
        note = input(txt)

        if note == 's':
            again = False
        else:
            if note != '':
                try:
                    if note.isnumeric():
                        v['rating'] = int(note)
                        clips.save_data()
                except:
                    print('rating failed')
        pos = (pos+1) % len(videos)


if __name__ == '__main__':
    rate_serie('10print')
