#!/usr/bin/env python
# coding: utf-8

"""get project from Project root directory"""

import os
import csv

ROOT = "/home/laurent/Projets"

# DEPRECATED
# projets rangés par année

# def get_all_project_dir(root=ROOT):
#     years = []
#     all_dirs = []
#     for f in os.listdir(root):
#         if os.path.isdir(os.path.join(root, f)):
#             print(f)
#             if is_year(f):
#                 years.append(f)
#             else:
#                 all_dirs.append({
#                     "dir": "root",
#                     "name": f
#                 })
#     for year in years:
#         all_dirs += get_year(root, year)
#     return all_dirs


# def get_year(root, year):
#     all_dirs = []
#     p = os.path.join(root, year)
#     for f in os.listdir(p):
#         if os.path.isdir(os.path.join(p, f)):
#             all_dirs.append({
#                 "dir": year,
#                 "name": f
#             })
#     return all_dirs


# def is_year(f):
#     if len(f) == 4 and f.isnumeric():
#         return True
#     return False


# def print_dirs(all_dirs):
#     d_dirs = {}
#     for d in all_dirs:
#         if d['dir'] in d_dirs:
#             d_dirs[d['dir']].append(d)
#         else:
#             d_dirs[d['dir']] = [d]
#     k = list(d_dirs.keys())
#     k.sort()
#     for i in k:
#         l = d_dirs[i]
#         projs = sorted(l, key=lambda e: e['name'])
#         for p in projs:
#             print(i, p['name'])


def read_dir_csv(filename):
    """
    Read project data from csv
    """
    with open(filename, encoding="utf-8") as csvfile:
        reader = csv.DictReader(csvfile)
        # header = reader.read()
        # print('header', ", ".join(header))

        entries = list(reader)
    for entry in entries:
        if entry['year'].isnumeric():
            entry['year'] = int(entry['year'])
        for col in ['tags', 'category']:
            if entry[col] != '':
                entry[col] = [i.strip() for i in entry[col].split(',')]
            else:
                entry[col] = []
    return entries


def check_entries(entries):
    """check if entries exists"""
    for e in entries:
        p = os.path.join(ROOT, e['folder'], e['name'])
        if not os.path.isdir(p):
            print('project not found', e['name'])


if __name__ == '__main__':
    projects = read_dir_csv('dirs.csv')
    # all_dirs = get_all_project_dir()
    # print_dirs(all_dirs)
    # for f in all_dirs:
    #     print('%s\t%s' % (f['dir'], f['name']))
