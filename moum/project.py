#!/usr/bin/python
# coding: utf-8

"""Project class for Moum"""

import os
import time

import json
from pathlib import Path
from dataclasses import dataclass, field
from typing import List, Literal

from moum.utils import get_mtime
from moum.utils import ask_for
from moum.walk_git import find_repos, get_repo_data
from moum.post import get_posts, create_post
from moum.schema import ElemData
from moum.command import navigate, run


@dataclass(repr=False)
class Project(ElemData):
    """Project class for Moum"""
    category: str = 'project'
    path: str = ''
    status: Literal['open', 'closed'] = 'open'
    parent_project: str = ''
    tags: list = field(default_factory=list)
    project_tags: List[str] = field(default_factory=list)
    repos_paths: List[str] = field(default_factory=list)
    posts_paths: List[str] = field(default_factory=list)
    year: str = ''
    org_id: str = ''
    run: str = ''
    _last_update: str = ''

    def __repr__(self):
        return f'< { self.category } :: { self.name } >'

    def __post_init__(self):
        self._st_mtime = None
        self._posts = []
        self._repos = []
        self._parent = None
        self._child_projects = []
        self._moum = None
        self._data_dir = os.path.join(self.path, "moum")
        self._data_path = os.path.join(self._data_dir, "moum-project.json")

    def set_moum(self, moum):
        """set moum main instance"""
        self._moum = moum

    def get_parent(self):
        """
        get parent object from parent_project name
        TODO duplicate get_child_projects
        """
        self._parent = None
        if self.parent_project:
            if self.parent_project in self._moum.projects_by_name:
                self._parent = self._moum.projects_by_name[self.parent_project]

    def to_json(self, path):
        """export static data to json"""
        data = self.serialize()
        with open(path, 'w', encoding='utf-8') as stream:
            json.dump(data, stream, indent=4, ensure_ascii=False)

    def save(self):
        """save to moum project folder"""
        path = Path(self._data_dir)
        if not path.is_dir():
            path.mkdir(parents=True)
        self.to_json(self._data_path)

    @staticmethod
    def from_json(path):
        """load from json"""
        if not Path(path).exists():
            print('Project not found: ', path)
            return None
        with open(path, 'r', encoding='utf-8') as stream:
            data = json.load(stream)
        return Project(**data)

    @staticmethod
    def load(project_path):
        """load from moum project folder"""
        moum_path = os.path.join(project_path, 'moum/moum-project.json')
        return Project.from_json(moum_path)

    def add_child_project(self, project):
        """add child project"""
        self._child_projects.append(project)

    def find_repos(self):
        """find personnal git repos in project folder"""
        repos = find_repos(self.path)
        for repo in repos:
            self.repos_paths.append(repo['path'])
        return repos

    def get_repos(self):
        """get repository data from .git/config"""
        self._repos = []
        if self.repos_paths:
            for repo_path in self.repos_paths:
                repo_data = get_repo_data(repo_path)
                if repo_data:
                    self._repos.append(repo_data)
        return self._repos

    def create_post(self):
        """create a new post"""
        self._posts.append(create_post(self, self._moum))

    def get_posts(self):
        """find posts in ./moum/posts folder"""
        post_folder = os.path.join(self._data_dir, 'posts')
        self._posts = get_posts(post_folder)
        self.posts_paths = [post.path for post in self._posts]
        return self._posts

    def get_main_post(self):
        """
        get main post of parent project if childre
        get post thas is 'project_post' otherwise
        """
        if self._parent:
            return self._parent.get_main_post()

        for post in self._posts:
            if post.post_type == 'project_post':
                return post

        return None

    def get_children_posts(self):
        """
        return all post from the project
        that are not the main project post
        and all posts from other children projects
        """
        children_posts = []
        for post in self._posts:
            if post.post_type == 'post':
                children_posts.append(post)
        for child_project in self._child_projects:
            children_posts += child_project.posts()

        children_posts = sorted(children_posts)
        return children_posts

    def post(self, id_post=None):
        """get post by id"""
        if len(self._posts) == 0:
            print('no posts')
            return None

        if len(self._posts) == 1:
            return self._posts[0]

        if isinstance(id_post, int) and id_post < len(self._posts):
            return self._posts[id_post]

        if isinstance(id_post, str):
            for post in self._posts:
                if id_post in post.name:
                    return post
        title = ask_for([p.title for p in self._posts])
        return [p for p in self._posts if p.title == title][0]

    def posts(self):
        """get all posts"""
        return self._posts

    def update(self):
        """WIP track changes"""
        self.get_posts()
        print('update')
        self._last_update = time.time()

    def check_change(self):
        """WIP track changes"""
        print('check')
        mtime_s = get_mtime(self._data_dir)
        self._st_mtime = mtime_s
        if mtime_s > self._last_update:
            return True
        return False

    def export_to_list(self):
        """export to moum project list"""
        return {
            "name": self.name,
            "path": self.path,
            "uid": self.uid
        }

    def navigate(self, app='emacs'):
        """open in navigator (default=emacs)"""
        navigate(self.path, app)

    def run_script(self):
        """run script"""
        if self.run:
            print(self._data_dir)
            print(self.run)
            run(self._data_dir, self.run)
