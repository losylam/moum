#!/bin/python
"""
Find repos in directory
"""

import os

import configparser

GIT_LST_GROUPS = ["ofxKlar",
                  "losylam",
                  "telescope-harmonique",
                  "camion-conversation",
                  "singularites-des-territoires",
                  "PlateformeC",
                  "enfrancaisedanslatexte"]

GIT_PATH = "/home/laurent/Git"


def is_mine_data(gitpath, lst_groups=None):
    """
    Get if the project is mine i.e. its group is in my list
    of groups
    """
    if not lst_groups:
        lst_groups = GIT_LST_GROUPS
    try:
        repo_data = get_repo_data(gitpath)
        if repo_data['project_group'] in lst_groups:
            repo_data['mine'] = True
        else:
            repo_data['mine'] = False
        return repo_data
    except:
        return False
    return False


def get_config(gitpath):
    """get config path"""
    config = configparser.ConfigParser()
    config.read(os.path.join(gitpath, '.git', 'config'), encoding="utf-8")
    return config


def get_remote(gitpath):
    """get remote by reading .git config file"""
    config = get_config(gitpath)
    if 'remote "origin"' in config:
        origin = config['remote "origin"']
        if "url" in origin:
            return origin["url"]
    return ""


def get_repo_data(gitpath):
    """get data from config file"""
    remote_url = get_remote(gitpath)
    if not remote_url:
        return None
    if remote_url.startswith("http"):
        elems = remote_url.split('//')[1].split('/')
        git_server = elems[0]
        project_path = elems[1] + "/" + elems[2]
        project_group = elems[1]
        name = elems[2].split('.')[0]
    else:
        git_server = remote_url.split('@')[1].split(':')[0]
        project_path = remote_url.split(':')[1]
        project_group = project_path.split("/")[0]
        name = project_path.split('/')[1].split('.')[0]

    return {
        "path": gitpath,
        "name": name,
        "remote_url": remote_url,
        "git_server": git_server,
        "project_path": project_path,
        "project_group": project_group
    }


def find_repos(folder):
    """recursively find repos inside folder"""
    tw = TreeWalker(folder)
    return tw.lst_mine


class TreeWalker:
    """
    Walk through a file tree and
    - get all git projects
    - sort them by groups
    """
    def __init__(self, root):
        self.root = root
        self.blacklist = ['.',
                          '.git',
                          'cache',
                          'test',
                          'venv',
                          'node_modules',
                          'lib',
                          '.pio',
                          '__pycache__',
                          'PlayOn']

        self.lst_all = []
        self.lst_mine = []
        self.lst_other = []
        self.lst_error = []

        self.repos = {}

        self.visited = []
        self.walk()

    def filter_reps(self, reps, root):
        """
        return directories that are not blacklisted
        """
        lst_out = []
        for rep in reps:
            if os.path.isdir(os.path.join(root, rep)):
                ok = True
                for f in self.blacklist:
                    ok = ok and not rep.startswith(f)
                if ok:
                    lst_out.append(os.path.join(root, rep))
        return lst_out

    def get_data(self, abs_rep):
        """
        get data from .git
        """
        try:
            data = is_mine_data(abs_rep)
            print(data)
            if data:
                if data['mine']:
                    self.lst_mine.append(data)
                else:
                    self.lst_other.append(data)
            else:
                self.lst_error.append(abs_rep)
                self.lst_all.append(data)
        except:
            print('fail reading .git/config', abs_rep)

    def walk_rep(self, abs_rep):
        """
        get data from abs_rep and continues inside directories
        """
        self.visited.append(abs_rep)
        lst = os.listdir(abs_rep)
        lst = self.filter_reps(lst, abs_rep)
        if '.git' in os.listdir(abs_rep):
            self.get_data(abs_rep)

        for f in lst:
            if f not in self.visited:
                try:
                    self.walk_rep(f)
                except:
                    print('fail walking', f)

    def walk(self):
        """walk next step"""
        self.walk_rep(self.root)
