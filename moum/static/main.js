/// UTILS ///
const lerp = (x, y, a) => x * (1 - a) + y * a;
const clamp = (a, min = 0, max = 1) => Math.min(max, Math.max(min, a));
const invlerp = (x, y, a) => clamp((a - x) / (y - x));
const range = (x1, y1, x2, y2, a) => lerp(x2, y2, invlerp(x1, y1, a));

/// MENU ///
let width = window.innerWidth;
let menu_visible = false;
let menu = document.querySelector('.mm-menu-container');
let menu_btn = document.querySelector('.mm-menu-btn');
let menu_icon = document.querySelector('.mm-menu-btn-i');

function url_param(){
    const params = new URLSearchParams(document.URL.split('?')[1]);
    if (params.has('menu')){
        show_menu();
    }
}
// url_param();


function toggle_menu(){
    if (menu_visible){
        hide_menu();
    }else{
        show_menu();
    }
}

function show_menu(){
    menu_visible = true;
    menu.classList.add('mm-visible');
    // menu.style.opacity = 1;
    menu_icon.classList.remove('ri-menu-line');
    menu_icon.classList.add('ri-close-line');
    console.log('show');
}

function hide_menu(){
    menu_visible = false;
    menu.classList.remove('mm-visible');
    menu_icon.classList.add('ri-menu-line');
    menu_icon.classList.remove('ri-close-line');
    console.log('hide');
}

menu_btn.addEventListener('click', ()=>{
    toggle_menu();
});

menu.addEventListener('click', ()=>{
    toggle_menu();
});

menu_btn.addEventListener('mouseover', ()=>{
    if (width<768) return;
    if (!menu_visible){
        show_menu();
    }
});

document.querySelector('header').addEventListener('mouseleave', ()=>{
    if (width<768) return;
    if (menu_visible){
        hide_menu();
    }
});

/// RESIZE IFRAME ///

let iframes = document.querySelectorAll('iframe:not(.mm-front-video)');

function resize_iframe(e){
    let parent = e.parentElement;
    let bb = parent.getBoundingClientRect();
    let w = bb.width - 24;
    let ratio = e.width/e.height;
    let h = w/ratio;
    e.width = w;
    e.height = h;
}

function resize_front_video(){
    let frame = document.querySelector('iframe.mm-front-video');
    if (frame == undefined) return;
    if (width > 1280) {
        let parent = frame.parentElement;
        let bb = parent.getBoundingClientRect();

        let w_parent = bb.width;
        let w = bb.height*16/9;
        frame.style.height = bb.height + 'px';
        frame.style.width = w + 'px';
        frame.style.left = - (w-w_parent)/2 + 'px';
    }else{
        frame.style.height = '';
        frame.style.width = '';
        frame.style.left = 'unset';
    }
}

function resize_iframes(){
    iframes.forEach(e => resize_iframe(e));
    resize_front_video();
}

resize_iframes();


window.addEventListener('resize', ()=>{
    resize_iframes();
    width = window.innerWidth;
});


/// MOBILE INTERFACE ///

/// option 1
function click_to_show(){
    let click_prevented = false;

    let links = document.querySelectorAll('.mm-item-over a');
    links.forEach(elem => {
        let elem_p = elem.parentNode;
        // elem.addEventListener('click', e => e.preventDefault());
        let old_click = ()=>{};
        elem.addEventListener('touchstart', e=>{
            console.log("touchstart");
            if (!click_prevented){
                links.forEach(ee => {
                    ee.addEventListener('click', e=>{
                        e.preventDefault();
                        // e.stopImmediatePropagation();
                    });
                });
                click_prevented = true;
            }
            if (!elem_p.classList.contains('mm-visible')){
                console.log('not visible');
                links.forEach(link => link.parentNode.classList.remove('mm-visible'));
                // elem_a.removeEventListener('click', elem_a.click);
                elem_p.classList.add('mm-visible');
            }else{
                console.log('visible');
            }
        });
        elem.addEventListener('touchend', e=>{
            console.log("touchend");
            // elem.click();
            // elem_a.addEventListener('click', old_click);
        });
    });
}

/// option 2
function scroll_to_show(){
    document.querySelectorAll('.mm-item-over').forEach(elem=>{
        elem.style.transition = 'none';
    });
    let elems = document.querySelectorAll('.mm-item');
    let elem_h = elems[0].getBoundingClientRect().height;
    let window_h = window.innerHeight;
    let range_0 = elem_h/4;
    let range_1 = range_0 + elem_h;
    let range_2 = range_1 + elem_h/4;
    // let range_3 = 6*elem_h/2;
    console.log(elem_h, window_h);
    document.addEventListener('scroll', e=>{
        // let elem = elems[0];
        elems.forEach(elem=>{
            let elem_bb = elem.getBoundingClientRect();
            let elem_y = elem_bb.top;
            if (elem_y<window_h){
                let op = 0;
                console.log('scroll', window.scrollY, elem_y);
                if (elem_y < range_0){
                    op = range(0, range_0, 0, 1, elem_y);
                }else if (elem_y < range_1){
                    op = 1;
                }else if (elem_y < range_2){
                    op = range(range_1, range_2, 1, 0, elem_y);
                }
                elem.querySelector('.mm-item-over').style.opacity = Math.pow(op, 2);
            }
        });
        //     let elem_y = elem.getBoundingClientRect().y;
        // })
    });
}

// let scroll_to_show_active = false;
// document.body.addEventListener('touchstart', ()=>{
//     if (!scroll_to_show_active){
//         scroll_to_show_active = true;
//         scroll_to_show();
//     }
// });

// option 3

function reset_overlay(elem){
    elem.querySelector('.mm-item-over').style.opacity = 0;
    elem.querySelector('.mm-item-over-info').style.pointerEvents = 'unset';
}

function button_to_show(){
    let items = document.querySelectorAll('.mm-item');
    items.forEach(item=>{
        let overlay = document.createElement('div');
        overlay.classList.add('mm-item-over-info');
        // overlay.textContent = 'informations';
        item.appendChild(overlay);

        overlay.addEventListener('click', ()=>{
            items.forEach(i => reset_overlay(i));
            overlay.style.pointerEvents = 'none';
            item.querySelector('.mm-item-over').style.opacity = 1;
        });
    });
    document.addEventListener('scroll', ()=>{
        items.forEach(i => reset_overlay(i));
    });
}

let button_to_show_active = false;
 document.body.addEventListener('touchstart', ()=>{
     if (!button_to_show_active){
        button_to_show_active = true;
        button_to_show();
    }
});
