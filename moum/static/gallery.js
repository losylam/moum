/// UTILS ///

function create(tag, parent, classes, attrs){
    let elem = document.createElement(tag);
    if (parent) parent.appendChild(elem);
    if (classes){
        if (typeof(classes) == 'string'){
            classes = classes.split(' ');
        }
        for (const cl of classes){
            elem.classList.add(cl);
        }
    }
    if (attrs){
        for (const [key, item] of Object.entries(attrs)){
            elem.setAttribute(key, item);
        }
    }
    return elem;
}

/// GALLERY ANIMATION ///
let anim_duration = 750;
let anim_n = 4;
let gallery_pos = 0;
let is_touch = false;
let anim = undefined;

let gallery = document.querySelector('.mm-gallery');
let gallery_bb = gallery.getBoundingClientRect();
let gallery_width = gallery_bb.width;

let items = document.querySelectorAll('.mm-gallery-item');
items.forEach(e=>{
    e.style.position = "absolute";
    e.style.top = "0px";
    e.style.left = "0px";
    e.style.display = "block";
    e.style.transform = `translateX(${ gallery_width  }px)`;
});
items[0].style.transform = `translateX(0px)`;

let gallery_overlay = create('div', gallery, 'mm-gallery-overlay');

let btns = create('div', gallery_overlay, 'mm-gallery-btns');
let prev_btn = create('button', btns, 'mm-gallery-btn mm-gallery-btn-nav mm-btn');
create('i', prev_btn, 'ri-arrow-left-double-fill');
let pause_btn = create('button', btns, 'mm-gallery-btn mm-gallery-btn-pause mm-btn');
let pause_icon = create('i', pause_btn, 'ri-pause-fill');
let next_btn = create('button', btns, 'mm-gallery-btn mm-gallery-btn-nav mm-btn');
create('i', next_btn, 'ri-arrow-right-double-fill');

// dont show controls if there is only one image
if (items.length<2){
    btns.style.display = 'none';
}

gallery_overlay.addEventListener('touchstart', ()=>{
    if (!is_touch){
        is_touch = true;
        gallery_overlay.addEventListener('click', ()=>{
            gallery_overlay.classList.add('mm-is-touch');
            toggle_anim();
        });
    }
});

function slide(item, show=false, dir='left'){
    let x_start = 0;
    let x_end = 0;
    let w = gallery_width;
    item.style.display = 'block';
    if (dir == 'left'){
        if (show){
            x_start = w;
        }else{
            x_end = -w;
        }
    }else{ // if right
        if (show){
            x_start = -w;
        }else{
            x_end = w;
        }
    }
    item.animate(
        [
            { transform: `translateX(${x_start}px)` },
            { transform: `translateX(${x_end}px)` }
        ],
        {
            fill: "forwards",
            duration: anim_duration,
            easing: "ease-in-out",
        }
    );
}


/*
 * Check if elem has one active animation
 * (not used)
 */
function is_animated(elem){
    let out = false;
    elem.getAnimations().forEach(e => {
        if (e.finished == false){
            out = true;
        }
    });
    return out;
}

function next(){
    slide(items[gallery_pos], false, 'left');
    gallery_pos = (gallery_pos+1) % items.length;
    slide(items[gallery_pos], true, 'left');
}

function prev(){
    slide(items[gallery_pos], false, 'right');
    gallery_pos = gallery_pos-1;
    if (gallery_pos < 0) gallery_pos = items.length - 1;
    slide(items[gallery_pos], true, 'right');
}

function start_anim(){
    anim = setInterval(()=> next(), anim_duration*anim_n);
    if (is_touch){
        pause_icon.classList.remove('ri-pause-fill');
        pause_icon.classList.add('ri-play-fill');
        gallery_overlay.style.opacity = 1;
        setTimeout(()=>gallery_overlay.style.opacity = 0, 700);
    }else{
        pause_icon.classList.remove('ri-play-fill');
        pause_icon.classList.add('ri-pause-fill');
    }
}

function stop_anim(){
    if (anim){
        clearInterval(anim);
        anim = undefined;
        if (is_touch){
            pause_icon.classList.add('ri-pause-fill');
            pause_icon.classList.remove('ri-play-fill');
            gallery_overlay.style.opacity = 1;
            setTimeout(()=>gallery_overlay.style.opacity = 0, 700);
        }else{
            pause_icon.classList.add('ri-play-fill');
            pause_icon.classList.remove('ri-pause-fill');
        }
    }
}

function toggle_anim(){
    if (anim) {
        stop_anim();
    }else{
        next();
        start_anim();
    }
}

if (items.length>=2){
    start_anim();
}


/// EVENTS ///

next_btn.addEventListener('click', ()=>{
    stop_anim();
    next();
});

prev_btn.addEventListener('click', ()=>{
    stop_anim();
    prev();
});

pause_btn.addEventListener('click', ()=>{
    if (! is_touch){
        toggle_anim();
    }
});


function swipe_event(elem, callback_left, callback_right){
    let start_x = 0;
    let has_moved = false;
    elem.addEventListener('touchstart', e=>{
        start_x = e.touches[0].clientX;
        has_moved = false;

    });
    elem.addEventListener('touchmove', e=>{
        if (!has_moved){
            let end_x = e.changedTouches[0].clientX;
            let diff = end_x - start_x;
            if (diff < -100){
                has_moved = true;
                callback_left();
            }else if (diff > 100){
                has_moved = true;
                callback_right();
            }
        }
    });
}

swipe_event(
    gallery_overlay,
    ()=>{
        stop_anim();
        next();
    },
    ()=>{
        stop_anim();
        prev();
    }
);

window.onresize = ()=>{
    // if (anim){
    //     stop_anim();
    // }
    gallery_bb = gallery.getBoundingClientRect();
    gallery_width = gallery_bb.width;
    items.forEach((e, i)=>{
        let anims = e.getAnimations();
        if (anims.length>0) {
            if (anims[0].playState != 'finished'){
                console.log(anims, anims[0]);
                anims[0].finish();
            }
        }
        if ((i != gallery_pos)){
            e.style.display = 'none';
            e.style.transform = `translateX(${ gallery_width }px)`;
        }else{
            e.style.display = 'block';
        }
    });
    // items[gallery_pos].style.transform = `translateX(0px)`;
};
