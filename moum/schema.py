#!/usr/bin/python
# coding: utf-8

"""
Existing data objects
- project.py: Project(ElemData)
- post.py: Post(ElemData)

Potential data object
- Repo
- Issue
- Tag
- Drawing / Collectible

Potential data Type
- path
- datetime
"""

from dataclasses import dataclass


@dataclass
class ElemData:
    """Generic data class for moum"""
    uid: str
    name: str
    label: str = ""
    description: str = ""
    category: str = ""

    def __repr__(self):
        return f'< { self.category } :: { self.name } >'

    def __str__(self):
        return f'{ self.category } :: { self.name }'

    # def __repr__(self):
    #     return '%s :: %s ::\n%s' % (
    #         self.category,
    #         self.name,
    #         pformat(self.__dict__)
    #     )

    def serialize(self):
        """create a serialized copy by removing dynamic attributes"""
        data = self.__dict__.copy()
        for key in list(data.keys()):
            if key.startswith('_'):
                data.pop(key)
        return data

# @dataclass
# class GitRepo(ElemData):
#     path: str = None
#     remote_url: str = None

# @dataclass
# class Tag(ElemData):
#     alias: List[str] = None
