#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Gitlab api wrapper for Moum"""

import gitlab
from colorama import Fore, Style

from .utils import ask_for
from .orgfile import OrgFile


class RemoteRepo:
    """Remote Gitlab repository"""
    def __init__(self, repo_data, config, moum):
        self.data = repo_data
        self.config = config
        self.moum = moum
        self.remote = None
        self.issues = []

        self.gitlab = gitlab.Gitlab(self.config["gitlab_address"],
                                    private_token=self.config["gitlab_token"])
        try:
            if 'gitlab_id' in repo_data:
                self.remote = self.get(repo_data['gitlab_id'])
            else:
                self.remote = self.get(repo_data["project_path"][:-4])
            self.milestones = self.remote.milestones.list(state="active")
        except:
            print('no connection')

    def get(self, data):
        """request from gitlab.project.get()"""
        return self.gitlab.projects.get(data)

    def update_repo(self):
        """update data from gitlab"""
        if 'gitlab_id' in self.data:
            self.remote = self.gitlab.projects.get(self.data['gitlab_id'])

    def get_gitlab_id(self):
        """get gitlab id"""
        return self.remote.attributes['id']

    def update_issues(self):
        """update issues from gitlab"""
        self.milestones = self.remote.milestones.list(state="active")
        self.issues = self.remote.issues.list(state="opened")
        if "issues" not in self.data:
            self.data["issues"] = {}
        for issue in self.issues:
            att = issue.attributes
            if att["iid"] not in self.data["issues"]:
                new_issue = {"id": att["iid"],
                             "title": att["title"],
                             "description": att["description"],
                             "state": att["state"],
                             "labels": att["labels"]}

                self.data["issues"][int(att["iid"])] = new_issue
        self.moum.save()

    def print_issues(self):
        """print issues"""
        self.update_issues()
        for issue in self.issues:
            att = issue.attributes
            txt = Fore.LIGHTRED_EX + f'[#{ att["iid"] }] '
            txt += Style.RESET_ALL + att['title']
            if att['milestone']:
                txt += f'{Fore.MAGENTA} {att["milestone"]["title"]} '
            print(txt)

    def print_milestones(self):
        """print milestones"""
        self.milestones = self.remote.milestones.list(state="active")
        for milestone in self.milestones:
            print(milestone.attributes['title'])

    def create_issue(self):
        """add a new issue"""
        title = input("title ? ")
        new_issue = {"title": title}
        if len(self.milestones) == 1:
            milestone = self.milestones[0].attributes

            add_or_not = input(f'add to milestone [{ milestone["title"] }]? ')
            if add_or_not:
                new_issue["milestone_id"] = milestone["id"]
        elif len(self.milestones) > 1:
            milestones = [m.attributes["title"] for m in self.milestones]
            choice = ask_for(milestones)
            choice = milestones.index(choice)
            milestone = self.milestones[int(choice)].attributes
            new_issue["milestone_id"] = milestone["id"]
        self.remote.issues.create(new_issue)

    def create_milestone(self):
        """add a new milestone"""
        title = input("title ? ")
        self.remote.milestones.create({"title": title})

    def write_to_file(self, filename):
        """create Orgfile with issues"""
        f = OrgFile(filename)
        f.create(self.data["issues"])
