#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Sync dictionnary with json file"""

import json
import os


class JsonDict(dict):
    """Json dict sync with a file"""
    def __init__(self, filename):
        if os.path.isfile(filename):
            with open(filename, 'r', encoding='utf-8') as f:
                data = json.load(f)
            super().__init__(data)
        else:
            super().__init__({})
        self.filename = filename

    def __setitem__(self, item, value):
        super().__setitem__(item, value)

        with open(self.filename, 'w', encoding='utf-8') as f:
            json.dump(self, f, indent=4)
