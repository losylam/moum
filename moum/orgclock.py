"""Parse orgclock data"""

from pathlib import Path

import orgparse

from moum.utils import time_to_json


def parse_org(project_org):
    """parse org file"""
    return orgparse.load(project_org)


def get_projects_from_org(project_org):
    """
    get all nodes tagged with "project"
    in -project_org-
    """
    pro = orgparse.load(project_org)
    project_list_nodes = [c for c in pro.children if "projet" in c.tags]
    project_nodes = []
    for project_list_node in project_list_nodes:
        project_nodes += project_list_node.children
    return project_nodes


def get_project_node(project_org, project_tag):
    """
    return the first node from -project_org-
    tagged -project_tag-
    """
    nodes = get_projects_from_org(project_org)
    for node in nodes:
        if get_project_tag(node) == project_tag:
            return node
    return None


def get_project_clocks(project_org, project_tag):
    """
    return all clocks from -project_org-
    tagged -project_tag-
    """
    node = get_project_node(project_org, project_tag)
    clocked = get_clocked(node)
    return clocked


def get_clock_projects(project_org):
    """
    return all clocks in -project_org-
    """
    nodes = get_projects_from_org(project_org)
    for node in nodes:
        tag = get_project_tag(node)
        if tag:
            clocked = get_clocked(node)
            for clock in clocked:
                clock['project'] = tag
            print('////////', tag)
            print(clocked)


def get_project_tag(node):
    """return the tag that is not 'projet' """
    tags = [tag for tag in node.tags if tag != 'projet']
    if len(tags) == 1:
        return tags[0]
    return None


def get_node_path(node):
    """
    get full path of -node-
    """
    parent = node.parent
    parents = []
    while parent is not None:
        parents.append(parent)
        parent = parent.parent
    parents.reverse()
    node_path = ''
    for parent in parents:
        node_path += parent.heading.strip() + '/'

    node_path += node.heading.strip()
    return node_path


def get_clocked(root_node):
    """
    get all clock from -root_node-
    """
    clocked = []
    for node in root_node:
        if not node.is_root():
            for clock in node.clock:
                clocked.append({
                    'path': get_node_path(node),
                    'start': clock.start.timestamp(),
                    'end': clock.end.timestamp(),
                })
    sorted_clocked = sorted(clocked, key=lambda x: x['start'])
    return sorted_clocked


def show_clock(clk, relative_to='', from_time='', to_time='9999'):
    """
    show clock data :
    - date
    - duration in minutes
    - path (relative to -relative_to-)
    """
    dur = (clk['end'] - clk['start'])/60
    dur = int(dur)
    time_s = time_to_json(clk['start'])
    dur_s = f'  ::: {dur:4} m. :::   '
    path = Path(clk['path']).relative_to(relative_to)
    if from_time < time_s < to_time:
        print(time_s, dur_s, path)
        return dur
    return 0


def clock_by_year(clk):
    """group clocks by year"""
    d = {}
    for y in range(2015, 2024, 1):
        tot = 0
        y_s = str(y)
        y_e = str(y+1)
        for c in clk:
            tot += show_clock(c, '/', y_s, y_e)
            d[y_s] = tot
