from pathlib import Path
from dataclasses import dataclass
# from typing import Optional

from moum.schema import ElemData


@dataclass
class Media:
    path: Path
    is_thumbnail: bool = False
    # post: ElemData = None
    post: ElemData = None
    exists: bool = False

    def __post_init__(self):
        if self.path.exists():
            self.exists = True
        else:
            print('media not found', self.path)

    # def export(self):
    #     pass
