#!/usr/bin/python
# coding: utf-8

"""Html templates utilities for Moum"""

import shutil
from pathlib import Path

from jinja2 import Environment, FileSystemLoader
# from cmarkgfm import github_flavored_markdown_to_html
from cmarkgfm import markdown_to_html
from cmarkgfm.cmark import Options as cmarkgfmOptions
from bs4 import BeautifulSoup

MOUM_ROOT = '/home/laurent/Projets/Moum/src/moum'

cmark_options = (
    cmarkgfmOptions.CMARK_OPT_UNSAFE
)

env = Environment(
    loader=FileSystemLoader([
        Path(MOUM_ROOT) / 'moum/templates',
        Path(MOUM_ROOT) / 'test/input/templates',
    ]),
    trim_blocks=True,
    lstrip_blocks=True
)


def gfm_to_html(content, remove_title=True):
    """translate markdown to html"""
    if remove_title:
        lines = [
            line
            for line in content.split('\n')
            if not line.startswith('# ')
        ]
        content = "\n".join(lines)
    # return github_flavored_markdown_to_html(content, cmark_options)
    return markdown_to_html(content, cmark_options)


def get_html_body(content):
    """get body content of an html page as string"""
    if '<body' in content:
        html_bs = BeautifulSoup(content, 'html.parser')
        body = html_bs.find('body')
        return body.decode_contents(0)
    return content


def apply_template(template_path, data=None):
    """
    get html with the template -template_path-
    passing -**data- to the template
    """
    template = env.get_template(template_path)
    if data:
        return template.render(**data)
    return template.render()


def export_template(template_path, data, path):
    """
    export html to -path- with the template -template_path-
    passing -**data- to the template
    """
    template = env.get_template(template_path)
    html = template.render(**data)

    export_html(html, path)


def export_html(html, path, soup=False, prettify=False):
    """write html content in a file"""
    html_out = html
    if prettify:
        html_out = BeautifulSoup(html, 'html.parser').prettify()
    elif soup:
        html_out = str(BeautifulSoup(html, 'html.parser'))
    with open(path, 'w', encoding='utf-8') as f:
        f.write(html_out)
    return True


def get_post_data(posts, lang=None):
    """Get a list of dicionnary for each post of -posts-"""
    items = []
    for post in posts:
        if post.frontpage:
            post_data = post.serialize()
            if post.thumbnail:
                link = str("/" / Path(post.link) / post.thumbnail)
                post_data['_thumbnail_link'] = link
            post_data['tags'] = post.get_tags(lang)
            items.append(post_data)

    if lang == 'en':
        items_en = []
        for item in items:
            if item['title_en']:
                item['title'] = item['title_en']
                item['link'] = 'en/' + item['link_en']
                item['description'] = item['description_en']
                items_en.append(item)
        return items_en

    return items


def export_index(moum, posts, path):
    """Export the front page"""
    print('export index')
    template = env.get_template("index.html")
    output = Path(path)
    root = moum.get_site_root()
    data = {
        'title': "Laurent Malys | Code creatif et bricolages numériques",
        'root': root,
        'description': "Art algorithmique, code créatif et fabrication numérique à Nantes",
        'menu': moum.menu['fr'],
        'lang': 'fr',
        'thumbnail_link': '/static/thumbnail2.jpg',
        'fulllink_en': root + '/en',
        'fulllink_fr': root + '/'
    }

    items = get_post_data(posts)
    html = template.render(data=data, items=items)
    export_html(html, output / 'index.html')

    export_index_en(moum, posts, path)


def export_index_en(moum, posts, path):
    """Export the front page in english"""
    template = env.get_template("index.html")
    output = Path(path) / 'en'
    root = moum.get_site_root()
    if not output.exists():
        output.mkdir(parents=True)
    data_en = {
        'title': "Laurent Malys | Creative technologist",
        'root': root,
        'description': "Old+New media artist and creative technologist based in Nantes, France",
        'menu': moum.menu['en'],
        'lang': 'en',
        'thumbnail_link': '/static/thumbnail2.jpg',
        'fulllink_en': root + '/en',
        'fulllink_fr': root + '/'
    }
    items_en = get_post_data(posts, lang='en')
    html = template.render(data=data_en, items=items_en)
    export_html(html, output / 'index.html')


def reload_static(path):
    """copy static files"""
    p_static = Path(path, 'static')
    if p_static.exists():
        shutil.rmtree(p_static)
    p_out = Path(path)
    shutil.copytree(Path(MOUM_ROOT) / 'moum/static/', p_out / 'static')
