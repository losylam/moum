
function get_label(node){
    let d_start = new Date(node.start*1000);
    let d_end = new Date(node.end*1000);
    let label = d_start.toLocaleString() + ' -> ' + d_end.toLocaleString() + ' ---- ' +  node.path;
    return label;
}


const container = d3.select('body').append('div');

d3.json('camion.json').then(data => {
    console.log('data', data);
    data.reverse();
    get_label(data[0]);
    container.selectAll('div')
        .data(data, d => d.path)
        .join(enter => enter.append('div')
              .text(d => get_label(d)));
});


